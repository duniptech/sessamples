package dodafEnt;
import GenCol.*;
import genDevs.modeling.*;
import simView.*;
import modeling.*;
import ops.*;

public class Activities extends ViewableAtomic{
	
	
	public Activities(){
		this("dodafEnt.Activities");
	}
	
	public Activities(String name){
		super(name);
		
			
	}
	
	public static void main(String... args){
		
		EntitySES activities = new EntitySES("dodafEnt.Activities");
		//show root in XML
		activities.info(activities.toXML());
		//show entity info for root
		activities.logEntityHierarchy();

		// write SES to file (default name is ses root entity name.xml in
		// project workspace)
		activities.writeSES();
		
	
	}
	
	
	
}	
