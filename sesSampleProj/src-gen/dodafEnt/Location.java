package dodafEnt;
import GenCol.*;
import genDevs.modeling.*;
import simView.*;
import modeling.*;
import ops.*;

public class Location extends ViewableAtomic{
	
	
	public Location(){
		this("dodafEnt.Location");
	}
	
	public Location(String name){
		super(name);
		
			
	}
	
	public static void main(String... args){
		
		EntitySES location = new EntitySES("dodafEnt.Location");
		//show root in XML
		location.info(location.toXML());
		//show entity info for root
		location.logEntityHierarchy();

		// write SES to file (default name is ses root entity name.xml in
		// project workspace)
		location.writeSES();
		
	
	}
	
	
	
}	
