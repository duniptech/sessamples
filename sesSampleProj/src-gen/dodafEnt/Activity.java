package dodafEnt;
import GenCol.*;
import genDevs.modeling.*;
import simView.*;
import modeling.*;
import ops.*;

public class Activity extends ViewableAtomic{
	
	
	public Activity(){
		this("dodafEnt.Activity");
	}
	
	public Activity(String name){
		super(name);
		
			
	}
	
	public static void main(String... args){
		
		EntitySES activity = new EntitySES("dodafEnt.Activity");
		//show root in XML
		activity.info(activity.toXML());
		//show entity info for root
		activity.logEntityHierarchy();

		// write SES to file (default name is ses root entity name.xml in
		// project workspace)
		activity.writeSES();
		
	
	}
	
	
	
}	
