package metaSystem;
import GenCol.*;
import genDevs.modeling.*;
import simView.*;
import modeling.*;
import ops.*;

public class System extends ViewableDigraph{
	
	
	public System(){
		this("metaSystem.System");
	}
	
	public System(String name){
		super(name);
		
			
	}
	
	public static void main(String... args){
		EntitySES skillmeasure = new EntitySES("metaPerson.SkillMeasure");
		EntitySES persontype = new EntitySES("metaPerson.PersonType");
		EntitySES skill = new EntitySES("metaPerson.Skill");
		EntitySES skillmeasures = new EntitySES("metaPerson.SkillMeasures");
		EntitySES materiel = new EntitySES("metaSystem.Materiel");
		
		EntitySES system = new EntitySES("metaSystem.System");
		
		MultiSpec systemmaterialma = new MultiSpec("metaSystem.systemMaterialMA", system);	
		system.addMultiSpecToEntity(systemmaterialma);
		system.addEntityToMultiSpec("metaSystem.systemMaterialMA",materiel);
		
		MultiSpec systempersontypema = new MultiSpec("metaSystem.systemPersonTypeMA", system);	
		system.addMultiSpecToEntity(systempersontypema);
		system.addEntityToMultiSpec("metaSystem.systemPersonTypeMA",persontype);
		
		Aspect skilldec_skill = new Aspect("metaPerson.skillDec", skill);
		system.addAspectToEntity(skilldec_skill);
		system.addEntityToAspect("metaPerson.skillDec",skillmeasures);	
		
		MultiSpec persontypema_persontype = new MultiSpec("metaPerson.personTypeMA", persontype);	
		system.addMultiSpecToEntity(persontypema_persontype);
		system.addEntityToMultiSpec("metaPerson.personTypeMA",skill);
		
		MultiSpec skillmeasurema_skillmeasures = new MultiSpec("metaPerson.skillMeasureMA", skillmeasures);	
		system.addMultiSpecToEntity(skillmeasurema_skillmeasures);
		system.addEntityToMultiSpec("metaPerson.skillMeasureMA",skillmeasure);
		//show root in XML
		system.info(system.toXML());
		//show entity info for root
		system.logEntityHierarchy();

		// write SES to file (default name is ses root entity name.xml in
		// project workspace)
		system.writeSES();
		
	
	}
	
	
	
}	
