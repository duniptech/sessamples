package metaSystem;
import GenCol.*;
import genDevs.modeling.*;
import simView.*;
import modeling.*;
import ops.*;

public class Materiel extends ViewableAtomic{
	
	
	public Materiel(){
		this("metaSystem.Materiel");
	}
	
	public Materiel(String name){
		super(name);
		
			
	}
	
	public static void main(String... args){
		
		EntitySES materiel = new EntitySES("metaSystem.Materiel");
		//show root in XML
		materiel.info(materiel.toXML());
		//show entity info for root
		materiel.logEntityHierarchy();

		// write SES to file (default name is ses root entity name.xml in
		// project workspace)
		materiel.writeSES();
		
	
	}
	
	
	
}	
