package metaSystem;
import GenCol.*;
import genDevs.modeling.*;
import simView.*;
import modeling.*;
import ops.*;

public class Service extends ViewableAtomic{
	
	
	public Service(){
		this("metaSystem.Service");
	}
	
	public Service(String name){
		super(name);
		
			
	}
	
	public static void main(String... args){
		
		EntitySES service = new EntitySES("metaSystem.Service");
		//show root in XML
		service.info(service.toXML());
		//show entity info for root
		service.logEntityHierarchy();

		// write SES to file (default name is ses root entity name.xml in
		// project workspace)
		service.writeSES();
		
	
	}
	
	
	
}	
