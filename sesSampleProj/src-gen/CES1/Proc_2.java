package CES1;
import GenCol.*;
import genDevs.modeling.*;
import simView.*;
import modeling.*;
import ops.*;

public class Proc_2 extends ViewableAtomic{
	
	public double procTime;
	
	public Proc_2(){
		this("CES1.Proc_2");
	}
	
	public Proc_2(String name){
		super(name);
		
		addInport("inProcessJob");
		addOutport("outProcessedJob");
			
		addTestInput("inProcessJob", new efpDataElements.Job());
		addTestInput("inProcessJob", new efpDataElements.Job(),1);
	}
	
	public static void main(String... args){
		
		EntitySES proc_2 = new EntitySES("CES1.Proc_2");
		//show root in XML
		proc_2.info(proc_2.toXML());
		//show entity info for root
		proc_2.logEntityHierarchy();

		// write SES to file (default name is ses root entity name.xml in
		// project workspace)
		proc_2.writeSES();
		
	
	}
	
	
	
}	
