package CES1;
import GenCol.*;
import genDevs.modeling.*;
import simView.*;
import modeling.*;
import ops.*;

public class EF extends ViewableDigraph{
	
	
	public EF(){
		this("CES1.EF");
	}
	
	public EF(String name){
		super(name);
		
		addInport("inEfstart");
		addInport("inSolved");
		addOutport("outGenerated");
		addOutport("outResult");
			
		addTestInput("inEfstart", new efpDataElements.Start());
		addTestInput("inEfstart", new efpDataElements.Start(),1);
		addTestInput("inSolved", new efpDataElements.Job());
		addTestInput("inSolved", new efpDataElements.Job(),1);
	}
	
	public static void main(String... args){
		EntitySES fixedinterval_genr = new EntitySES("CES1.FixedInterval_Genr");
		EntitySES transd = new EntitySES("CES1.Transd");
		
		EntitySES ef = new EntitySES("CES1.EF");
		
		Aspect efdec = new Aspect("CES1.efDec", ef);
		ef.addAspectToEntity(efdec);
		ef.addEntityToAspect("CES1.efDec",fixedinterval_genr);	
		ef.addEntityToAspect("CES1.efDec",transd);	
		//show root in XML
		ef.info(ef.toXML());
		//show entity info for root
		ef.logEntityHierarchy();

		// write SES to file (default name is ses root entity name.xml in
		// project workspace)
		ef.writeSES();
		
	
	}
	
	
	
}	
