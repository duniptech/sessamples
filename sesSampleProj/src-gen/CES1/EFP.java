package CES1;
import GenCol.*;
import genDevs.modeling.*;
import simView.*;
import modeling.*;
import ops.*;

public class EFP extends ViewableDigraph{
	
	
	public EFP(){
		this("CES1.EFP");
	}
	
	public EFP(String name){
		super(name);
		
		addInport("inEfpstart");
		addOutport("outResult");
			
		addTestInput("inEfpstart", new efpDataElements.Start());
		addTestInput("inEfpstart", new efpDataElements.Start(),1);
	}
	
	public static void main(String... args){
		EntitySES fixedinterval_genr = new EntitySES("CES1.FixedInterval_Genr");
		EntitySES proc_1 = new EntitySES("CES1.Proc_1");
		EntitySES proc_2 = new EntitySES("CES1.Proc_2");
		EntitySES transd = new EntitySES("CES1.Transd");
		EntitySES ef = new EntitySES("CES1.EF");
		
		EntitySES efp = new EntitySES("CES1.EFP");
		
		Aspect efpdec = new Aspect("CES1.efpDec", efp);
		efp.addAspectToEntity(efpdec);
		efp.addEntityToAspect("CES1.efpDec",proc_1);	
		efp.addEntityToAspect("CES1.efpDec",proc_2);	
		efp.addEntityToAspect("CES1.efpDec",ef);	
		
		Aspect efdec_ef = new Aspect("CES1.efDec", ef);
		efp.addAspectToEntity(efdec_ef);
		efp.addEntityToAspect("CES1.efDec",fixedinterval_genr);	
		efp.addEntityToAspect("CES1.efDec",transd);	
		//show root in XML
		efp.info(efp.toXML());
		//show entity info for root
		efp.logEntityHierarchy();

		// write SES to file (default name is ses root entity name.xml in
		// project workspace)
		efp.writeSES();
		
	
	}
	
	
	
}	
