package CES1;
import GenCol.*;
import genDevs.modeling.*;
import simView.*;
import modeling.*;
import ops.*;

public class FixedInterval_Genr extends ViewableAtomic{
	
	public double period;
	public int jobId;
	
	public FixedInterval_Genr(){
		this("CES1.FixedInterval_Genr");
	}
	
	public FixedInterval_Genr(String name){
		super(name);
		
		addInport("inGenstart");
		addInport("inGenstop");
		addOutport("outJob");
			
		addTestInput("inGenstart", new efpDataElements.Start());
		addTestInput("inGenstart", new efpDataElements.Start(),1);
		addTestInput("inGenstop", new efpDataElements.Stop());
		addTestInput("inGenstop", new efpDataElements.Stop(),1);
	}
	
	public static void main(String... args){
		
		EntitySES fixedinterval_genr = new EntitySES("CES1.FixedInterval_Genr");
		//show root in XML
		fixedinterval_genr.info(fixedinterval_genr.toXML());
		//show entity info for root
		fixedinterval_genr.logEntityHierarchy();

		// write SES to file (default name is ses root entity name.xml in
		// project workspace)
		fixedinterval_genr.writeSES();
		
	
	}
	
	
	
}	
