package CES1;
import GenCol.*;
import genDevs.modeling.*;
import simView.*;
import modeling.*;
import ops.*;

public class Proc_1 extends ViewableAtomic{
	
	public double procTime;
	
	public Proc_1(){
		this("CES1.Proc_1");
	}
	
	public Proc_1(String name){
		super(name);
		
		addInport("inProcessJob");
		addOutport("outProcessedJob");
			
		addTestInput("inProcessJob", new efpDataElements.Job());
		addTestInput("inProcessJob", new efpDataElements.Job(),1);
	}
	
	public static void main(String... args){
		
		EntitySES proc_1 = new EntitySES("CES1.Proc_1");
		//show root in XML
		proc_1.info(proc_1.toXML());
		//show entity info for root
		proc_1.logEntityHierarchy();

		// write SES to file (default name is ses root entity name.xml in
		// project workspace)
		proc_1.writeSES();
		
	
	}
	
	
	
}	
