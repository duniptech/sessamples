package metaPorts;
import GenCol.*;
import genDevs.modeling.*;
import simView.*;
import modeling.*;
import ops.*;

public class Port extends ViewableDigraph{
	
	
	public Port(){
		this("metaPorts.Port");
	}
	
	public Port(String name){
		super(name);
		
			
	}
	
	public static void main(String... args){
		EntitySES serviceport = new EntitySES("metaPorts.ServicePort");
		
		EntitySES port = new EntitySES("metaPorts.Port");
		
		Spec portspec = new Spec("metaPorts.portSpec", port);
		port.addSpecToEntity(portspec);
		port.addEntityToSpec("metaPorts.portSpec",serviceport);
		//show root in XML
		port.info(port.toXML());
		//show entity info for root
		port.logEntityHierarchy();

		// write SES to file (default name is ses root entity name.xml in
		// project workspace)
		port.writeSES();
		
	
	}
	
	
	
}	
