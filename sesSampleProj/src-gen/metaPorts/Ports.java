package metaPorts;
import GenCol.*;
import genDevs.modeling.*;
import simView.*;
import modeling.*;
import ops.*;

public class Ports extends ViewableDigraph{
	
	
	public Ports(){
		this("metaPorts.Ports");
	}
	
	public Ports(String name){
		super(name);
		
			
	}
	
	public static void main(String... args){
		EntitySES serviceport = new EntitySES("metaPorts.ServicePort");
		EntitySES port = new EntitySES("metaPorts.Port");
		
		EntitySES ports = new EntitySES("metaPorts.Ports");
		
		MultiSpec portsma = new MultiSpec("metaPorts.portsMA", ports);	
		ports.addMultiSpecToEntity(portsma);
		ports.addEntityToMultiSpec("metaPorts.portsMA",port);
		
		Spec portspec_port = new Spec("metaPorts.portSpec", port);
		ports.addSpecToEntity(portspec_port);
		ports.addEntityToSpec("metaPorts.portSpec",serviceport);
		//show root in XML
		ports.info(ports.toXML());
		//show entity info for root
		ports.logEntityHierarchy();

		// write SES to file (default name is ses root entity name.xml in
		// project workspace)
		ports.writeSES();
		
	
	}
	
	
	
}	
