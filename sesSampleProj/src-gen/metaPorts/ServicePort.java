package metaPorts;
import GenCol.*;
import genDevs.modeling.*;
import simView.*;
import modeling.*;
import ops.*;

public class ServicePort extends ViewableAtomic{
	
	
	public ServicePort(){
		this("metaPorts.ServicePort");
	}
	
	public ServicePort(String name){
		super(name);
		
			
	}
	
	public static void main(String... args){
		
		EntitySES serviceport = new EntitySES("metaPorts.ServicePort");
		//show root in XML
		serviceport.info(serviceport.toXML());
		//show entity info for root
		serviceport.logEntityHierarchy();

		// write SES to file (default name is ses root entity name.xml in
		// project workspace)
		serviceport.writeSES();
		
	
	}
	
	
	
}	
