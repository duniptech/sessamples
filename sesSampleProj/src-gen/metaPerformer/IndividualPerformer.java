package metaPerformer;
import GenCol.*;
import genDevs.modeling.*;
import simView.*;
import modeling.*;
import ops.*;

public class IndividualPerformer extends ViewableDigraph{
	
	
	public IndividualPerformer(){
		this("metaPerformer.IndividualPerformer");
	}
	
	public IndividualPerformer(String name){
		super(name);
		
			
	}
	
	public static void main(String... args){
		EntitySES organization = new EntitySES("metaOrganization.Organization");
		
		EntitySES individualperformer = new EntitySES("metaPerformer.IndividualPerformer");
		
		Spec individualperformerspec = new Spec("metaPerformer.individualPerformerSpec", individualperformer);
		individualperformer.addSpecToEntity(individualperformerspec);
		individualperformer.addEntityToSpec("metaPerformer.individualPerformerSpec",organization);
		//show root in XML
		individualperformer.info(individualperformer.toXML());
		//show entity info for root
		individualperformer.logEntityHierarchy();

		// write SES to file (default name is ses root entity name.xml in
		// project workspace)
		individualperformer.writeSES();
		
	
	}
	
	
	
}	
