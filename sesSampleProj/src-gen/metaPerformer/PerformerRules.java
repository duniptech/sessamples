package metaPerformer;
import GenCol.*;
import genDevs.modeling.*;
import simView.*;
import modeling.*;
import ops.*;

public class PerformerRules extends ViewableAtomic{
	
	
	public PerformerRules(){
		this("metaPerformer.PerformerRules");
	}
	
	public PerformerRules(String name){
		super(name);
		
			
	}
	
	public static void main(String... args){
		
		EntitySES performerrules = new EntitySES("metaPerformer.PerformerRules");
		//show root in XML
		performerrules.info(performerrules.toXML());
		//show entity info for root
		performerrules.logEntityHierarchy();

		// write SES to file (default name is ses root entity name.xml in
		// project workspace)
		performerrules.writeSES();
		
	
	}
	
	
	
}	
