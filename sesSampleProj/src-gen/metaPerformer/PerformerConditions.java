package metaPerformer;
import GenCol.*;
import genDevs.modeling.*;
import simView.*;
import modeling.*;
import ops.*;

public class PerformerConditions extends ViewableAtomic{
	
	
	public PerformerConditions(){
		this("metaPerformer.PerformerConditions");
	}
	
	public PerformerConditions(String name){
		super(name);
		
			
	}
	
	public static void main(String... args){
		
		EntitySES performerconditions = new EntitySES("metaPerformer.PerformerConditions");
		//show root in XML
		performerconditions.info(performerconditions.toXML());
		//show entity info for root
		performerconditions.logEntityHierarchy();

		// write SES to file (default name is ses root entity name.xml in
		// project workspace)
		performerconditions.writeSES();
		
	
	}
	
	
	
}	
