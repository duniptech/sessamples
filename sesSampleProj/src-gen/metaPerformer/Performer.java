package metaPerformer;
import GenCol.*;
import genDevs.modeling.*;
import simView.*;
import modeling.*;
import ops.*;

public class Performer extends ViewableDigraph{
	
	
	public Performer(){
		this("metaPerformer.Performer");
	}
	
	public Performer(String name){
		super(name);
		
			
	}
	
	public static void main(String... args){
		EntitySES individualperformer = new EntitySES("metaPerformer.IndividualPerformer");
		EntitySES activities = new EntitySES("dodafEnt.Activities");
		EntitySES persontype = new EntitySES("metaPerson.PersonType");
		EntitySES location = new EntitySES("dodafEnt.Location");
		EntitySES serviceport = new EntitySES("metaPorts.ServicePort");
		EntitySES organization = new EntitySES("metaOrganization.Organization");
		EntitySES performerrules = new EntitySES("metaPerformer.PerformerRules");
		EntitySES port = new EntitySES("metaPorts.Port");
		EntitySES skill = new EntitySES("metaPerson.Skill");
		EntitySES skillmeasures = new EntitySES("metaPerson.SkillMeasures");
		EntitySES ports = new EntitySES("metaPorts.Ports");
		EntitySES skillmeasure = new EntitySES("metaPerson.SkillMeasure");
		EntitySES service = new EntitySES("metaSystem.Service");
		EntitySES system = new EntitySES("metaSystem.System");
		EntitySES performerconditions = new EntitySES("metaPerformer.PerformerConditions");
		EntitySES organizationtype = new EntitySES("metaOrganization.OrganizationType");
		EntitySES materiel = new EntitySES("metaSystem.Materiel");
		
		EntitySES performer = new EntitySES("metaPerformer.Performer");
		
		Aspect performerdec = new Aspect("metaPerformer.performerDec", performer);
		performer.addAspectToEntity(performerdec);
		performer.addEntityToAspect("metaPerformer.performerDec",activities);	
		performer.addEntityToAspect("metaPerformer.performerDec",location);	
		performer.addEntityToAspect("metaPerformer.performerDec",performerrules);	
		performer.addEntityToAspect("metaPerformer.performerDec",performerconditions);	
		
		Spec performerspec = new Spec("metaPerformer.performerSpec", performer);
		performer.addSpecToEntity(performerspec);
		performer.addEntityToSpec("metaPerformer.performerSpec",ports);
		performer.addEntityToSpec("metaPerformer.performerSpec",persontype);
		performer.addEntityToSpec("metaPerformer.performerSpec",service);
		performer.addEntityToSpec("metaPerformer.performerSpec",system);
		performer.addEntityToSpec("metaPerformer.performerSpec",organizationtype);
		
		MultiSpec perfomerma = new MultiSpec("metaPerformer.perfomerMA", performer);	
		performer.addMultiSpecToEntity(perfomerma);
		performer.addEntityToMultiSpec("metaPerformer.perfomerMA",individualperformer);
		
		Aspect skilldec_skill = new Aspect("metaPerson.skillDec", skill);
		performer.addAspectToEntity(skilldec_skill);
		performer.addEntityToAspect("metaPerson.skillDec",skillmeasures);	
		
		MultiSpec persontypema_persontype = new MultiSpec("metaPerson.personTypeMA", persontype);	
		performer.addMultiSpecToEntity(persontypema_persontype);
		performer.addEntityToMultiSpec("metaPerson.personTypeMA",skill);
		
		MultiSpec skillmeasurema_skillmeasures = new MultiSpec("metaPerson.skillMeasureMA", skillmeasures);	
		performer.addMultiSpecToEntity(skillmeasurema_skillmeasures);
		performer.addEntityToMultiSpec("metaPerson.skillMeasureMA",skillmeasure);
		
		MultiSpec portsma_ports = new MultiSpec("metaPorts.portsMA", ports);	
		performer.addMultiSpecToEntity(portsma_ports);
		performer.addEntityToMultiSpec("metaPorts.portsMA",port);
		
		MultiSpec systemmaterialma_system = new MultiSpec("metaSystem.systemMaterialMA", system);	
		performer.addMultiSpecToEntity(systemmaterialma_system);
		performer.addEntityToMultiSpec("metaSystem.systemMaterialMA",materiel);
		
		MultiSpec systempersontypema_system = new MultiSpec("metaSystem.systemPersonTypeMA", system);	
		performer.addMultiSpecToEntity(systempersontypema_system);
		performer.addEntityToMultiSpec("metaSystem.systemPersonTypeMA",persontype);
		
		Spec individualperformerspec_individualperformer = new Spec("metaPerformer.individualPerformerSpec", individualperformer);
		performer.addSpecToEntity(individualperformerspec_individualperformer);
		performer.addEntityToSpec("metaPerformer.individualPerformerSpec",organization);
		
		Spec portspec_port = new Spec("metaPorts.portSpec", port);
		performer.addSpecToEntity(portspec_port);
		performer.addEntityToSpec("metaPorts.portSpec",serviceport);
		//show root in XML
		performer.info(performer.toXML());
		//show entity info for root
		performer.logEntityHierarchy();

		// write SES to file (default name is ses root entity name.xml in
		// project workspace)
		performer.writeSES();
		
	
	}
	
	
	
}	
