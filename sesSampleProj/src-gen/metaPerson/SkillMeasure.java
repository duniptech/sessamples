package metaPerson;
import GenCol.*;
import genDevs.modeling.*;
import simView.*;
import modeling.*;
import ops.*;

public class SkillMeasure extends ViewableAtomic{
	
	
	public SkillMeasure(){
		this("metaPerson.SkillMeasure");
	}
	
	public SkillMeasure(String name){
		super(name);
		
			
	}
	
	public static void main(String... args){
		
		EntitySES skillmeasure = new EntitySES("metaPerson.SkillMeasure");
		//show root in XML
		skillmeasure.info(skillmeasure.toXML());
		//show entity info for root
		skillmeasure.logEntityHierarchy();

		// write SES to file (default name is ses root entity name.xml in
		// project workspace)
		skillmeasure.writeSES();
		
	
	}
	
	
	
}	
