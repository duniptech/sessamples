package metaPerson;
import GenCol.*;
import genDevs.modeling.*;
import simView.*;
import modeling.*;
import ops.*;

public class SkillMeasures extends ViewableDigraph{
	
	
	public SkillMeasures(){
		this("metaPerson.SkillMeasures");
	}
	
	public SkillMeasures(String name){
		super(name);
		
			
	}
	
	public static void main(String... args){
		EntitySES skillmeasure = new EntitySES("metaPerson.SkillMeasure");
		
		EntitySES skillmeasures = new EntitySES("metaPerson.SkillMeasures");
		
		MultiSpec skillmeasurema = new MultiSpec("metaPerson.skillMeasureMA", skillmeasures);	
		skillmeasures.addMultiSpecToEntity(skillmeasurema);
		skillmeasures.addEntityToMultiSpec("metaPerson.skillMeasureMA",skillmeasure);
		//show root in XML
		skillmeasures.info(skillmeasures.toXML());
		//show entity info for root
		skillmeasures.logEntityHierarchy();

		// write SES to file (default name is ses root entity name.xml in
		// project workspace)
		skillmeasures.writeSES();
		
	
	}
	
	
	
}	
