package metaPerson;
import GenCol.*;
import genDevs.modeling.*;
import simView.*;
import modeling.*;
import ops.*;

public class PersonType extends ViewableDigraph{
	
	
	public PersonType(){
		this("metaPerson.PersonType");
	}
	
	public PersonType(String name){
		super(name);
		
			
	}
	
	public static void main(String... args){
		EntitySES skillmeasure = new EntitySES("metaPerson.SkillMeasure");
		EntitySES skill = new EntitySES("metaPerson.Skill");
		EntitySES skillmeasures = new EntitySES("metaPerson.SkillMeasures");
		
		EntitySES persontype = new EntitySES("metaPerson.PersonType");
		
		MultiSpec persontypema = new MultiSpec("metaPerson.personTypeMA", persontype);	
		persontype.addMultiSpecToEntity(persontypema);
		persontype.addEntityToMultiSpec("metaPerson.personTypeMA",skill);
		
		Aspect skilldec_skill = new Aspect("metaPerson.skillDec", skill);
		persontype.addAspectToEntity(skilldec_skill);
		persontype.addEntityToAspect("metaPerson.skillDec",skillmeasures);	
		
		MultiSpec skillmeasurema_skillmeasures = new MultiSpec("metaPerson.skillMeasureMA", skillmeasures);	
		persontype.addMultiSpecToEntity(skillmeasurema_skillmeasures);
		persontype.addEntityToMultiSpec("metaPerson.skillMeasureMA",skillmeasure);
		//show root in XML
		persontype.info(persontype.toXML());
		//show entity info for root
		persontype.logEntityHierarchy();

		// write SES to file (default name is ses root entity name.xml in
		// project workspace)
		persontype.writeSES();
		
	
	}
	
	
	
}	
