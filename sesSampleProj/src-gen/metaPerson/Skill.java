package metaPerson;
import GenCol.*;
import genDevs.modeling.*;
import simView.*;
import modeling.*;
import ops.*;

public class Skill extends ViewableDigraph{
	
	
	public Skill(){
		this("metaPerson.Skill");
	}
	
	public Skill(String name){
		super(name);
		
			
	}
	
	public static void main(String... args){
		EntitySES skillmeasure = new EntitySES("metaPerson.SkillMeasure");
		EntitySES skillmeasures = new EntitySES("metaPerson.SkillMeasures");
		
		EntitySES skill = new EntitySES("metaPerson.Skill");
		
		Aspect skilldec = new Aspect("metaPerson.skillDec", skill);
		skill.addAspectToEntity(skilldec);
		skill.addEntityToAspect("metaPerson.skillDec",skillmeasures);	
		
		MultiSpec skillmeasurema_skillmeasures = new MultiSpec("metaPerson.skillMeasureMA", skillmeasures);	
		skill.addMultiSpecToEntity(skillmeasurema_skillmeasures);
		skill.addEntityToMultiSpec("metaPerson.skillMeasureMA",skillmeasure);
		//show root in XML
		skill.info(skill.toXML());
		//show entity info for root
		skill.logEntityHierarchy();

		// write SES to file (default name is ses root entity name.xml in
		// project workspace)
		skill.writeSES();
		
	
	}
	
	
	
}	
