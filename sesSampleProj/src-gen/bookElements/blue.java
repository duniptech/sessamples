package bookElements;
import GenCol.*;
import genDevs.modeling.*;
import simView.*;
import modeling.*;
import ops.*;

public class blue extends ViewableAtomic{
	
	public String type;
	
	public blue(){
		this("bookElements.blue");
	}
	
	public blue(String name){
		super(name);
		
			
	}
	
	public static void main(String... args){
		
		EntitySES blue = new EntitySES("bookElements.blue");
		//show root in XML
		blue.info(blue.toXML());
		//show entity info for root
		blue.logEntityHierarchy();

		// write SES to file (default name is ses root entity name.xml in
		// project workspace)
		blue.writeSES();
		
	
	}
	
	
	
}	
