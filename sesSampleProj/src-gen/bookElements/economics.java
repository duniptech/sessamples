package bookElements;
import GenCol.*;
import genDevs.modeling.*;
import simView.*;
import modeling.*;
import ops.*;

public class economics extends ViewableAtomic{
	
	
	public economics(){
		this("bookElements.economics");
	}
	
	public economics(String name){
		super(name);
		
			
	}
	
	public static void main(String... args){
		
		EntitySES economics = new EntitySES("bookElements.economics");
		//show root in XML
		economics.info(economics.toXML());
		//show entity info for root
		economics.logEntityHierarchy();

		// write SES to file (default name is ses root entity name.xml in
		// project workspace)
		economics.writeSES();
		
	
	}
	
	
	
}	
