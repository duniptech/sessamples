package bookElements;
import GenCol.*;
import genDevs.modeling.*;
import simView.*;
import modeling.*;
import ops.*;

public class backCover extends ViewableDigraph{
	
	
	public backCover(){
		this("bookElements.backCover");
	}
	
	public backCover(String name){
		super(name);
		
			
	}
	
	public static void main(String... args){
		EntitySES blue = new EntitySES("bookElements.blue");
		EntitySES red = new EntitySES("bookElements.red");
		
		EntitySES backcover = new EntitySES("bookElements.backCover");
		
		Spec colorspec = new Spec("bookElements.colorSpec", backcover);
		backcover.addSpecToEntity(colorspec);
		backcover.addEntityToSpec("bookElements.colorSpec",blue);
		backcover.addEntityToSpec("bookElements.colorSpec",red);
		//show root in XML
		backcover.info(backcover.toXML());
		//show entity info for root
		backcover.logEntityHierarchy();

		// write SES to file (default name is ses root entity name.xml in
		// project workspace)
		backcover.writeSES();
		
	
	}
	
	
	
}	
