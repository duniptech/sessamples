package bookElements;
import GenCol.*;
import genDevs.modeling.*;
import simView.*;
import modeling.*;
import ops.*;

public class science extends ViewableAtomic{
	
	
	public science(){
		this("bookElements.science");
	}
	
	public science(String name){
		super(name);
		
			
	}
	
	public static void main(String... args){
		
		EntitySES science = new EntitySES("bookElements.science");
		//show root in XML
		science.info(science.toXML());
		//show entity info for root
		science.logEntityHierarchy();

		// write SES to file (default name is ses root entity name.xml in
		// project workspace)
		science.writeSES();
		
	
	}
	
	
	
}	
