package bookElements;
import GenCol.*;
import genDevs.modeling.*;
import simView.*;
import modeling.*;
import ops.*;

public class page extends ViewableAtomic{
	
	
	public page(){
		this("bookElements.page");
	}
	
	public page(String name){
		super(name);
		
			
	}
	
	public static void main(String... args){
		
		EntitySES page = new EntitySES("bookElements.page");
		//show root in XML
		page.info(page.toXML());
		//show entity info for root
		page.logEntityHierarchy();

		// write SES to file (default name is ses root entity name.xml in
		// project workspace)
		page.writeSES();
		
	
	}
	
	
	
}	
