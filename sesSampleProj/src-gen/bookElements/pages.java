package bookElements;
import GenCol.*;
import genDevs.modeling.*;
import simView.*;
import modeling.*;
import ops.*;

public class pages extends ViewableDigraph{
	
	
	public pages(){
		this("bookElements.pages");
	}
	
	public pages(String name){
		super(name);
		
			
	}
	
	public static void main(String... args){
		EntitySES page = new EntitySES("bookElements.page");
		
		EntitySES pages = new EntitySES("bookElements.pages");
		
		MultiSpec multipagespec = new MultiSpec("bookElements.multiPageSpec", pages);	
		pages.addMultiSpecToEntity(multipagespec);
		pages.addEntityToMultiSpec("bookElements.multiPageSpec",page);
		//show root in XML
		pages.info(pages.toXML());
		//show entity info for root
		pages.logEntityHierarchy();

		// write SES to file (default name is ses root entity name.xml in
		// project workspace)
		pages.writeSES();
		
	
	}
	
	
	
}	
