package bookElements;
import GenCol.*;
import genDevs.modeling.*;
import simView.*;
import modeling.*;
import ops.*;

public class frontCover extends ViewableDigraph{
	
	
	public frontCover(){
		this("bookElements.frontCover");
	}
	
	public frontCover(String name){
		super(name);
		
			
	}
	
	public static void main(String... args){
		EntitySES blue = new EntitySES("bookElements.blue");
		EntitySES red = new EntitySES("bookElements.red");
		
		EntitySES frontcover = new EntitySES("bookElements.frontCover");
		
		Spec colorspec = new Spec("bookElements.colorSpec", frontcover);
		frontcover.addSpecToEntity(colorspec);
		frontcover.addEntityToSpec("bookElements.colorSpec",blue);
		frontcover.addEntityToSpec("bookElements.colorSpec",red);
		//show root in XML
		frontcover.info(frontcover.toXML());
		//show entity info for root
		frontcover.logEntityHierarchy();

		// write SES to file (default name is ses root entity name.xml in
		// project workspace)
		frontcover.writeSES();
		
	
	}
	
	
	
}	
