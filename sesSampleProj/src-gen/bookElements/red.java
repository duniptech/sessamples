package bookElements;
import GenCol.*;
import genDevs.modeling.*;
import simView.*;
import modeling.*;
import ops.*;

public class red extends ViewableAtomic{
	
	public String type;
	
	public red(){
		this("bookElements.red");
	}
	
	public red(String name){
		super(name);
		
			
	}
	
	public static void main(String... args){
		
		EntitySES red = new EntitySES("bookElements.red");
		//show root in XML
		red.info(red.toXML());
		//show entity info for root
		red.logEntityHierarchy();

		// write SES to file (default name is ses root entity name.xml in
		// project workspace)
		red.writeSES();
		
	
	}
	
	
	
}	
