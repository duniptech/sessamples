package bookElements;
import GenCol.*;
import genDevs.modeling.*;
import simView.*;
import modeling.*;
import ops.*;

public class literature extends ViewableAtomic{
	
	
	public literature(){
		this("bookElements.literature");
	}
	
	public literature(String name){
		super(name);
		
			
	}
	
	public static void main(String... args){
		
		EntitySES literature = new EntitySES("bookElements.literature");
		//show root in XML
		literature.info(literature.toXML());
		//show entity info for root
		literature.logEntityHierarchy();

		// write SES to file (default name is ses root entity name.xml in
		// project workspace)
		literature.writeSES();
		
	
	}
	
	
	
}	
