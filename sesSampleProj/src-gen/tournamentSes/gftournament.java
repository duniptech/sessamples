package tournamentSes;
import GenCol.*;
import genDevs.modeling.*;
import simView.*;
import modeling.*;
import ops.*;

public class gftournament extends ViewableDigraph{
	
	public double duration;
	
	public gftournament(){
		this("tournamentSes.gftournament");
	}
	
	public gftournament(String name){
		super(name);
		
			
	}
	
	public static void main(String... args){
		EntitySES longer = new EntitySES("tournamentElements.longer");
		EntitySES draw = new EntitySES("tournamentElements.draw");
		EntitySES mental_states = new EntitySES("tournamentElements.mental_states");
		EntitySES rounds = new EntitySES("tournamentElements.rounds");
		EntitySES hole = new EntitySES("tournamentElements.hole");
		EntitySES shorter = new EntitySES("tournamentElements.shorter");
		EntitySES terrain = new EntitySES("tournamentElements.terrain");
		EntitySES away = new EntitySES("tournamentElements.away");
		EntitySES sw = new EntitySES("tournamentElements.SW");
		EntitySES anxious = new EntitySES("tournamentElements.anxious");
		EntitySES clubs = new EntitySES("tournamentElements.clubs");
		EntitySES confident = new EntitySES("tournamentElements.confident");
		EntitySES shots = new EntitySES("tournamentElements.shots");
		EntitySES slope = new EntitySES("tournamentElements.slope");
		EntitySES round = new EntitySES("tournamentElements.round");
		EntitySES club = new EntitySES("tournamentElements.club");
		EntitySES shot = new EntitySES("tournamentElements.shot");
		EntitySES lie = new EntitySES("tournamentElements.lie");
		EntitySES players = new EntitySES("tournamentElements.players");
		EntitySES backspin = new EntitySES("tournamentElements.backspin");
		EntitySES mental_state = new EntitySES("tournamentElements.mental_state");
		EntitySES amateur = new EntitySES("tournamentElements.amateur");
		EntitySES even = new EntitySES("tournamentElements.even");
		EntitySES rough = new EntitySES("tournamentElements.rough");
		EntitySES strategies = new EntitySES("tournamentElements.strategies");
		EntitySES strategy = new EntitySES("tournamentElements.strategy");
		EntitySES onewood = new EntitySES("tournamentElements.OneWood");
		EntitySES fairway = new EntitySES("tournamentElements.fairway");
		EntitySES player = new EntitySES("tournamentElements.player");
		EntitySES too = new EntitySES("tournamentElements.too");
		EntitySES shot_length = new EntitySES("tournamentElements.shot_length");
		EntitySES punch = new EntitySES("tournamentElements.punch");
		EntitySES caddy = new EntitySES("tournamentElements.caddy");
		EntitySES professional = new EntitySES("tournamentElements.professional");
		
		EntitySES gftournament = new EntitySES("tournamentSes.gftournament");
		
		Aspect tournamentstructure = new Aspect("tournamentElements.tournamentStructure", gftournament);
		gftournament.addAspectToEntity(tournamentstructure);
		gftournament.addEntityToAspect("tournamentElements.tournamentStructure",players);	
		gftournament.addEntityToAspect("tournamentElements.tournamentStructure",rounds);	
		gftournament.addEntityToAspect("tournamentElements.tournamentStructure",terrain);	
		
		Aspect terrainstructure_terrain = new Aspect("tournamentElements.terrainStructure", terrain);
		gftournament.addAspectToEntity(terrainstructure_terrain);
		gftournament.addEntityToAspect("tournamentElements.terrainStructure",lie);	
		gftournament.addEntityToAspect("tournamentElements.terrainStructure",shot_length);	
		gftournament.addEntityToAspect("tournamentElements.terrainStructure",slope);	
		
		Aspect playerstructure_player = new Aspect("tournamentElements.playerStructure", player);
		gftournament.addAspectToEntity(playerstructure_player);
		gftournament.addEntityToAspect("tournamentElements.playerStructure",caddy);	
		gftournament.addEntityToAspect("tournamentElements.playerStructure",mental_states);	
		gftournament.addEntityToAspect("tournamentElements.playerStructure",strategies);	
		
		Aspect caddystructure_caddy = new Aspect("tournamentElements.caddyStructure", caddy);
		gftournament.addAspectToEntity(caddystructure_caddy);
		gftournament.addEntityToAspect("tournamentElements.caddyStructure",clubs);	
		
		MultiSpec multimentalstates_mental_states = new MultiSpec("tournamentElements.multiMentalStates", mental_states);	
		gftournament.addMultiSpecToEntity(multimentalstates_mental_states);
		gftournament.addEntityToMultiSpec("tournamentElements.multiMentalStates",mental_state);
		
		MultiSpec multirounds_rounds = new MultiSpec("tournamentElements.multiRounds", rounds);	
		gftournament.addMultiSpecToEntity(multirounds_rounds);
		gftournament.addEntityToMultiSpec("tournamentElements.multiRounds",round);
		
		MultiSpec multihole_hole = new MultiSpec("tournamentElements.multiHole", hole);	
		gftournament.addMultiSpecToEntity(multihole_hole);
		gftournament.addEntityToMultiSpec("tournamentElements.multiHole",shots);
		
		MultiSpec multiclubs_clubs = new MultiSpec("tournamentElements.multiClubs", clubs);	
		gftournament.addMultiSpecToEntity(multiclubs_clubs);
		gftournament.addEntityToMultiSpec("tournamentElements.multiClubs",club);
		
		MultiSpec multishots_shots = new MultiSpec("tournamentElements.multiShots", shots);	
		gftournament.addMultiSpecToEntity(multishots_shots);
		gftournament.addEntityToMultiSpec("tournamentElements.multiShots",shot);
		
		MultiSpec multiround_round = new MultiSpec("tournamentElements.multiRound", round);	
		gftournament.addMultiSpecToEntity(multiround_round);
		gftournament.addEntityToMultiSpec("tournamentElements.multiRound",hole);
		
		MultiSpec multispecplayer_players = new MultiSpec("tournamentElements.multiSpecPlayer", players);	
		gftournament.addMultiSpecToEntity(multispecplayer_players);
		gftournament.addEntityToMultiSpec("tournamentElements.multiSpecPlayer",player);
		
		MultiSpec multistrategy_strategies = new MultiSpec("tournamentElements.multiStrategy", strategies);	
		gftournament.addMultiSpecToEntity(multistrategy_strategies);
		gftournament.addEntityToMultiSpec("tournamentElements.multiStrategy",strategy);
		
		Spec slopespec_slope = new Spec("tournamentElements.slopeSpec", slope);
		gftournament.addSpecToEntity(slopespec_slope);
		gftournament.addEntityToSpec("tournamentElements.slopeSpec",even);
		gftournament.addEntityToSpec("tournamentElements.slopeSpec",away);
		gftournament.addEntityToSpec("tournamentElements.slopeSpec",too);
		
		Spec clubspec_club = new Spec("tournamentElements.clubSpec", club);
		gftournament.addSpecToEntity(clubspec_club);
		gftournament.addEntityToSpec("tournamentElements.clubSpec",onewood);
		gftournament.addEntityToSpec("tournamentElements.clubSpec",sw);
		
		Spec shot_type_shot = new Spec("tournamentElements.shot_type", shot);
		gftournament.addSpecToEntity(shot_type_shot);
		gftournament.addEntityToSpec("tournamentElements.shot_type",longer);
		gftournament.addEntityToSpec("tournamentElements.shot_type",shorter);
		
		Spec liespec_lie = new Spec("tournamentElements.lieSpec", lie);
		gftournament.addSpecToEntity(liespec_lie);
		gftournament.addEntityToSpec("tournamentElements.lieSpec",fairway);
		gftournament.addEntityToSpec("tournamentElements.lieSpec",rough);
		
		Spec mentalstatespec_mental_state = new Spec("tournamentElements.mentalStateSpec", mental_state);
		gftournament.addSpecToEntity(mentalstatespec_mental_state);
		gftournament.addEntityToSpec("tournamentElements.mentalStateSpec",anxious);
		gftournament.addEntityToSpec("tournamentElements.mentalStateSpec",confident);
		
		Spec strategyspec_strategy = new Spec("tournamentElements.strategySpec", strategy);
		gftournament.addSpecToEntity(strategyspec_strategy);
		gftournament.addEntityToSpec("tournamentElements.strategySpec",draw);
		gftournament.addEntityToSpec("tournamentElements.strategySpec",backspin);
		gftournament.addEntityToSpec("tournamentElements.strategySpec",punch);
		
		Spec playerspec_player = new Spec("tournamentElements.playerSpec", player);
		gftournament.addSpecToEntity(playerspec_player);
		gftournament.addEntityToSpec("tournamentElements.playerSpec",professional);
		gftournament.addEntityToSpec("tournamentElements.playerSpec",amateur);
		
		Spec shotlengthspec_shot_length = new Spec("tournamentElements.shotLengthSpec", shot_length);
		gftournament.addSpecToEntity(shotlengthspec_shot_length);
		gftournament.addEntityToSpec("tournamentElements.shotLengthSpec",longer);
		gftournament.addEntityToSpec("tournamentElements.shotLengthSpec",shorter);
		//show root in XML
		gftournament.info(gftournament.toXML());
		//show entity info for root
		gftournament.logEntityHierarchy();

		// write SES to file (default name is ses root entity name.xml in
		// project workspace)
		gftournament.writeSES();
		
	
	}
	
	
	
}	
