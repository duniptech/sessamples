package metaOrganization;
import GenCol.*;
import genDevs.modeling.*;
import simView.*;
import modeling.*;
import ops.*;

public class Organization extends ViewableAtomic{
	
	
	public Organization(){
		this("metaOrganization.Organization");
	}
	
	public Organization(String name){
		super(name);
		
			
	}
	
	public static void main(String... args){
		
		EntitySES organization = new EntitySES("metaOrganization.Organization");
		//show root in XML
		organization.info(organization.toXML());
		//show entity info for root
		organization.logEntityHierarchy();

		// write SES to file (default name is ses root entity name.xml in
		// project workspace)
		organization.writeSES();
		
	
	}
	
	
	
}	
