package metaOrganization;
import GenCol.*;
import genDevs.modeling.*;
import simView.*;
import modeling.*;
import ops.*;

public class OrganizationType extends ViewableAtomic{
	
	
	public OrganizationType(){
		this("metaOrganization.OrganizationType");
	}
	
	public OrganizationType(String name){
		super(name);
		
			
	}
	
	public static void main(String... args){
		
		EntitySES organizationtype = new EntitySES("metaOrganization.OrganizationType");
		//show root in XML
		organizationtype.info(organizationtype.toXML());
		//show entity info for root
		organizationtype.logEntityHierarchy();

		// write SES to file (default name is ses root entity name.xml in
		// project workspace)
		organizationtype.writeSES();
		
	
	}
	
	
	
}	
