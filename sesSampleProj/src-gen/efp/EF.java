package efp;
import GenCol.*;
import genDevs.modeling.*;
import simView.*;
import modeling.*;
import ops.*;

public class EF extends ViewableDigraph{
	
	
	public EF(){
		this("efp.EF");
	}
	
	public EF(String name){
		super(name);
		
		addInport("inSolved");
		addInport("inGenerated");
		addOutport("outResult");
			
		addTestInput("inSolved", new efpDataElements.Job());
		addTestInput("inSolved", new efpDataElements.Job(),1);
		addTestInput("inGenerated", new efpDataElements.Job());
		addTestInput("inGenerated", new efpDataElements.Job(),1);
	}
	
	public static void main(String... args){
		EntitySES randominterval = new EntitySES("efp.RandomInterval");
		EntitySES genr = new EntitySES("efp.Genr");
		EntitySES fixedinterval = new EntitySES("efp.FixedInterval");
		EntitySES transd = new EntitySES("efp.Transd");
		
		EntitySES ef = new EntitySES("efp.EF");
		
		Aspect efdec = new Aspect("efp.efDec", ef);
		ef.addAspectToEntity(efdec);
		ef.addEntityToAspect("efp.efDec",genr);	
		ef.addEntityToAspect("efp.efDec",transd);	
		
		Spec genspec_genr = new Spec("efp.genSpec", genr);
		ef.addSpecToEntity(genspec_genr);
		ef.addEntityToSpec("efp.genSpec",randominterval);
		ef.addEntityToSpec("efp.genSpec",fixedinterval);
		//show root in XML
		ef.info(ef.toXML());
		//show entity info for root
		ef.logEntityHierarchy();

		// write SES to file (default name is ses root entity name.xml in
		// project workspace)
		ef.writeSES();
		
	
	}
	
	
	
}	
