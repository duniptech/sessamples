package efp;
import GenCol.*;
import genDevs.modeling.*;
import simView.*;
import modeling.*;
import ops.*;

public class Proc extends ViewableAtomic{
	
	public double procTime;
	
	public Proc(){
		this("efp.Proc");
	}
	
	public Proc(String name){
		super(name);
		
		addInport("inProcessJob");
		addOutport("outProcessedJob");
			
		addTestInput("inProcessJob", new efpDataElements.Job());
		addTestInput("inProcessJob", new efpDataElements.Job(),1);
	}
	
	public static void main(String... args){
		
		EntitySES proc = new EntitySES("efp.Proc");
		//show root in XML
		proc.info(proc.toXML());
		//show entity info for root
		proc.logEntityHierarchy();

		// write SES to file (default name is ses root entity name.xml in
		// project workspace)
		proc.writeSES();
		
	
	}
	
	
	
}	
