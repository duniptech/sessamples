package efp;
import GenCol.*;
import genDevs.modeling.*;
import simView.*;
import modeling.*;
import ops.*;

public class ProcBank extends ViewableDigraph{
	
	public int numProcs;
	
	public ProcBank(){
		this("efp.ProcBank");
	}
	
	public ProcBank(String name){
		super(name);
		
			
	}
	
	public static void main(String... args){
		EntitySES proc = new EntitySES("efp.Proc");
		
		EntitySES procbank = new EntitySES("efp.ProcBank");
		
		MultiSpec procbankmultidec = new MultiSpec("efp.procBankMultiDec", procbank);	
		procbank.addMultiSpecToEntity(procbankmultidec);
		procbank.addEntityToMultiSpec("efp.procBankMultiDec",proc);
		//show root in XML
		procbank.info(procbank.toXML());
		//show entity info for root
		procbank.logEntityHierarchy();

		// write SES to file (default name is ses root entity name.xml in
		// project workspace)
		procbank.writeSES();
		
	
	}
	
	
	
}	
