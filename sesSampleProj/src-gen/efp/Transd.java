package efp;
import GenCol.*;
import genDevs.modeling.*;
import simView.*;
import modeling.*;
import ops.*;

public class Transd extends ViewableAtomic{
	
	public double obsPeriod;
	public int lastJobSolved;
	public double throughput;
	public int arrived;
	
	public Transd(){
		this("efp.Transd");
	}
	
	public Transd(String name){
		super(name);
		
		addInport("inArrivedJob");
		addInport("inSolvedJob");
		addOutport("outResult");
		addOutport("outStop");
			
		addTestInput("inArrivedJob", new efpDataElements.Job());
		addTestInput("inArrivedJob", new efpDataElements.Job(),1);
		addTestInput("inSolvedJob", new efpDataElements.Job());
		addTestInput("inSolvedJob", new efpDataElements.Job(),1);
	}
	
	public static void main(String... args){
		
		EntitySES transd = new EntitySES("efp.Transd");
		//show root in XML
		transd.info(transd.toXML());
		//show entity info for root
		transd.logEntityHierarchy();

		// write SES to file (default name is ses root entity name.xml in
		// project workspace)
		transd.writeSES();
		
	
	}
	
	
	
}	
