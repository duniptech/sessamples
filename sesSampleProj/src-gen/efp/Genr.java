package efp;
import GenCol.*;
import genDevs.modeling.*;
import simView.*;
import modeling.*;
import ops.*;

public class Genr extends ViewableDigraph{
	
	public double period;
	public int jobId;
	
	public Genr(){
		this("efp.Genr");
	}
	
	public Genr(String name){
		super(name);
		
		addInport("inGenstart");
		addInport("inGenstop");
		addOutport("outJob");
			
		addTestInput("inGenstart", new efpDataElements.Start());
		addTestInput("inGenstart", new efpDataElements.Start(),1);
		addTestInput("inGenstop", new efpDataElements.Stop());
		addTestInput("inGenstop", new efpDataElements.Stop(),1);
	}
	
	public static void main(String... args){
		EntitySES randominterval = new EntitySES("efp.RandomInterval");
		EntitySES fixedinterval = new EntitySES("efp.FixedInterval");
		
		EntitySES genr = new EntitySES("efp.Genr");
		
		Spec genspec = new Spec("efp.genSpec", genr);
		genr.addSpecToEntity(genspec);
		genr.addEntityToSpec("efp.genSpec",randominterval);
		genr.addEntityToSpec("efp.genSpec",fixedinterval);
		//show root in XML
		genr.info(genr.toXML());
		//show entity info for root
		genr.logEntityHierarchy();

		// write SES to file (default name is ses root entity name.xml in
		// project workspace)
		genr.writeSES();
		
	
	}
	
	
	
}	
