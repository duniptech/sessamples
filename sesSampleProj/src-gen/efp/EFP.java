package efp;
import GenCol.*;
import genDevs.modeling.*;
import simView.*;
import modeling.*;
import ops.*;

public class EFP extends ViewableDigraph{
	
	
	public EFP(){
		this("efp.EFP");
	}
	
	public EFP(String name){
		super(name);
		
		addInport("inEfpstart");
		addOutport("outResult");
			
		addTestInput("inEfpstart", new efpDataElements.Start());
		addTestInput("inEfpstart", new efpDataElements.Start(),1);
	}
	
	public static void main(String... args){
		EntitySES ef = new EntitySES("efp.EF");
		EntitySES proc = new EntitySES("efp.Proc");
		EntitySES randominterval = new EntitySES("efp.RandomInterval");
		EntitySES procbank = new EntitySES("efp.ProcBank");
		EntitySES genr = new EntitySES("efp.Genr");
		EntitySES fixedinterval = new EntitySES("efp.FixedInterval");
		EntitySES transd = new EntitySES("efp.Transd");
		
		EntitySES efp = new EntitySES("efp.EFP");
		
		Aspect efpdec = new Aspect("efp.efpDec", efp);
		efp.addAspectToEntity(efpdec);
		efp.addEntityToAspect("efp.efpDec",ef);	
		efp.addEntityToAspect("efp.efpDec",procbank);	
		
		Aspect efdec_ef = new Aspect("efp.efDec", ef);
		efp.addAspectToEntity(efdec_ef);
		efp.addEntityToAspect("efp.efDec",genr);	
		efp.addEntityToAspect("efp.efDec",transd);	
		
		MultiSpec procbankmultidec_procbank = new MultiSpec("efp.procBankMultiDec", procbank);	
		efp.addMultiSpecToEntity(procbankmultidec_procbank);
		efp.addEntityToMultiSpec("efp.procBankMultiDec",proc);
		
		Spec genspec_genr = new Spec("efp.genSpec", genr);
		efp.addSpecToEntity(genspec_genr);
		efp.addEntityToSpec("efp.genSpec",randominterval);
		efp.addEntityToSpec("efp.genSpec",fixedinterval);
		//show root in XML
		efp.info(efp.toXML());
		//show entity info for root
		efp.logEntityHierarchy();

		// write SES to file (default name is ses root entity name.xml in
		// project workspace)
		efp.writeSES();
		
	
	}
	
	
	
}	
