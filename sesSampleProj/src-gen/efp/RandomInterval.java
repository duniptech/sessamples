package efp;
import GenCol.*;
import genDevs.modeling.*;
import simView.*;
import modeling.*;
import ops.*;

public class RandomInterval extends ViewableAtomic{
	
	public double seed;
	
	public RandomInterval(){
		this("efp.RandomInterval");
	}
	
	public RandomInterval(String name){
		super(name);
		
			
	}
	
	public static void main(String... args){
		
		EntitySES randominterval = new EntitySES("efp.RandomInterval");
		//show root in XML
		randominterval.info(randominterval.toXML());
		//show entity info for root
		randominterval.logEntityHierarchy();

		// write SES to file (default name is ses root entity name.xml in
		// project workspace)
		randominterval.writeSES();
		
	
	}
	
	
	
}	
