package efp;
import GenCol.*;
import genDevs.modeling.*;
import simView.*;
import modeling.*;
import ops.*;

public class FixedInterval extends ViewableAtomic{
	
	
	public FixedInterval(){
		this("efp.FixedInterval");
	}
	
	public FixedInterval(String name){
		super(name);
		
			
	}
	
	public static void main(String... args){
		
		EntitySES fixedinterval = new EntitySES("efp.FixedInterval");
		//show root in XML
		fixedinterval.info(fixedinterval.toXML());
		//show entity info for root
		fixedinterval.logEntityHierarchy();

		// write SES to file (default name is ses root entity name.xml in
		// project workspace)
		fixedinterval.writeSES();
		
	
	}
	
	
	
}	
