package bookSes;
import GenCol.*;
import genDevs.modeling.*;
import simView.*;
import modeling.*;
import ops.*;

public class book extends ViewableDigraph{
	
	
	public book(){
		this("bookSes.book");
	}
	
	public book(String name){
		super(name);
		
			
	}
	
	public static void main(String... args){
		EntitySES backcover = new EntitySES("bookElements.backCover");
		EntitySES literature = new EntitySES("bookElements.literature");
		EntitySES blue = new EntitySES("bookElements.blue");
		EntitySES red = new EntitySES("bookElements.red");
		EntitySES science = new EntitySES("bookElements.science");
		EntitySES frontcover = new EntitySES("bookElements.frontCover");
		EntitySES page = new EntitySES("bookElements.page");
		EntitySES pages = new EntitySES("bookElements.pages");
		
		EntitySES book = new EntitySES("bookSes.book");
		
		Spec booktypespec = new Spec("bookElements.bookTypeSpec", book);
		book.addSpecToEntity(booktypespec);
		book.addEntityToSpec("bookElements.bookTypeSpec",literature);
		book.addEntityToSpec("bookElements.bookTypeSpec",science);
		
		Aspect physicaldes = new Aspect("bookElements.physicalDes", book);
		book.addAspectToEntity(physicaldes);
		book.addEntityToAspect("bookElements.physicalDes",backcover);	
		book.addEntityToAspect("bookElements.physicalDes",frontcover);	
		book.addEntityToAspect("bookElements.physicalDes",pages);	
		
		MultiSpec multipagespec_pages = new MultiSpec("bookElements.multiPageSpec", pages);	
		book.addMultiSpecToEntity(multipagespec_pages);
		book.addEntityToMultiSpec("bookElements.multiPageSpec",page);
		
		Spec colorspec_backcover = new Spec("bookElements.colorSpec", backcover);
		book.addSpecToEntity(colorspec_backcover);
		book.addEntityToSpec("bookElements.colorSpec",blue);
		book.addEntityToSpec("bookElements.colorSpec",red);
		
		Spec colorspec_frontcover = new Spec("bookElements.colorSpec", frontcover);
		book.addSpecToEntity(colorspec_frontcover);
		book.addEntityToSpec("bookElements.colorSpec",blue);
		book.addEntityToSpec("bookElements.colorSpec",red);
		//show root in XML
		book.info(book.toXML());
		//show entity info for root
		book.logEntityHierarchy();

		// write SES to file (default name is ses root entity name.xml in
		// project workspace)
		book.writeSES();
		
	
	}
	
	
	
}	
