package tournamentElements;
import GenCol.*;
import genDevs.modeling.*;
import simView.*;
import modeling.*;
import ops.*;

public class mental_state extends ViewableDigraph{
	
	
	public mental_state(){
		this("tournamentElements.mental_state");
	}
	
	public mental_state(String name){
		super(name);
		
			
	}
	
	public static void main(String... args){
		EntitySES anxious = new EntitySES("tournamentElements.anxious");
		EntitySES confident = new EntitySES("tournamentElements.confident");
		
		EntitySES mental_state = new EntitySES("tournamentElements.mental_state");
		
		Spec mentalstatespec = new Spec("tournamentElements.mentalStateSpec", mental_state);
		mental_state.addSpecToEntity(mentalstatespec);
		mental_state.addEntityToSpec("tournamentElements.mentalStateSpec",anxious);
		mental_state.addEntityToSpec("tournamentElements.mentalStateSpec",confident);
		//show root in XML
		mental_state.info(mental_state.toXML());
		//show entity info for root
		mental_state.logEntityHierarchy();

		// write SES to file (default name is ses root entity name.xml in
		// project workspace)
		mental_state.writeSES();
		
	
	}
	
	
	
}	
