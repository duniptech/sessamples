package tournamentElements;
import GenCol.*;
import genDevs.modeling.*;
import simView.*;
import modeling.*;
import ops.*;

public class round extends ViewableDigraph{
	
	
	public round(){
		this("tournamentElements.round");
	}
	
	public round(String name){
		super(name);
		
			
	}
	
	public static void main(String... args){
		EntitySES longer = new EntitySES("tournamentElements.longer");
		EntitySES shot = new EntitySES("tournamentElements.shot");
		EntitySES hole = new EntitySES("tournamentElements.hole");
		EntitySES shorter = new EntitySES("tournamentElements.shorter");
		EntitySES shots = new EntitySES("tournamentElements.shots");
		
		EntitySES round = new EntitySES("tournamentElements.round");
		
		MultiSpec multiround = new MultiSpec("tournamentElements.multiRound", round);	
		round.addMultiSpecToEntity(multiround);
		round.addEntityToMultiSpec("tournamentElements.multiRound",hole);
		
		MultiSpec multihole_hole = new MultiSpec("tournamentElements.multiHole", hole);	
		round.addMultiSpecToEntity(multihole_hole);
		round.addEntityToMultiSpec("tournamentElements.multiHole",shots);
		
		MultiSpec multishots_shots = new MultiSpec("tournamentElements.multiShots", shots);	
		round.addMultiSpecToEntity(multishots_shots);
		round.addEntityToMultiSpec("tournamentElements.multiShots",shot);
		
		Spec shot_type_shot = new Spec("tournamentElements.shot_type", shot);
		round.addSpecToEntity(shot_type_shot);
		round.addEntityToSpec("tournamentElements.shot_type",longer);
		round.addEntityToSpec("tournamentElements.shot_type",shorter);
		//show root in XML
		round.info(round.toXML());
		//show entity info for root
		round.logEntityHierarchy();

		// write SES to file (default name is ses root entity name.xml in
		// project workspace)
		round.writeSES();
		
	
	}
	
	
	
}	
