package tournamentElements;
import GenCol.*;
import genDevs.modeling.*;
import simView.*;
import modeling.*;
import ops.*;

public class clubs extends ViewableDigraph{
	
	
	public clubs(){
		this("tournamentElements.clubs");
	}
	
	public clubs(String name){
		super(name);
		
			
	}
	
	public static void main(String... args){
		EntitySES club = new EntitySES("tournamentElements.club");
		EntitySES onewood = new EntitySES("tournamentElements.OneWood");
		EntitySES sw = new EntitySES("tournamentElements.SW");
		
		EntitySES clubs = new EntitySES("tournamentElements.clubs");
		
		MultiSpec multiclubs = new MultiSpec("tournamentElements.multiClubs", clubs);	
		clubs.addMultiSpecToEntity(multiclubs);
		clubs.addEntityToMultiSpec("tournamentElements.multiClubs",club);
		
		Spec clubspec_club = new Spec("tournamentElements.clubSpec", club);
		clubs.addSpecToEntity(clubspec_club);
		clubs.addEntityToSpec("tournamentElements.clubSpec",onewood);
		clubs.addEntityToSpec("tournamentElements.clubSpec",sw);
		//show root in XML
		clubs.info(clubs.toXML());
		//show entity info for root
		clubs.logEntityHierarchy();

		// write SES to file (default name is ses root entity name.xml in
		// project workspace)
		clubs.writeSES();
		
	
	}
	
	
	
}	
