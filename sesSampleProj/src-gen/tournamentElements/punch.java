package tournamentElements;
import GenCol.*;
import genDevs.modeling.*;
import simView.*;
import modeling.*;
import ops.*;

public class punch extends ViewableAtomic{
	
	
	public punch(){
		this("tournamentElements.punch");
	}
	
	public punch(String name){
		super(name);
		
			
	}
	
	public static void main(String... args){
		
		EntitySES punch = new EntitySES("tournamentElements.punch");
		//show root in XML
		punch.info(punch.toXML());
		//show entity info for root
		punch.logEntityHierarchy();

		// write SES to file (default name is ses root entity name.xml in
		// project workspace)
		punch.writeSES();
		
	
	}
	
	
	
}	
