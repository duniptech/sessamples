package tournamentElements;
import GenCol.*;
import genDevs.modeling.*;
import simView.*;
import modeling.*;
import ops.*;

public class professional extends ViewableAtomic{
	
	
	public professional(){
		this("tournamentElements.professional");
	}
	
	public professional(String name){
		super(name);
		
			
	}
	
	public static void main(String... args){
		
		EntitySES professional = new EntitySES("tournamentElements.professional");
		//show root in XML
		professional.info(professional.toXML());
		//show entity info for root
		professional.logEntityHierarchy();

		// write SES to file (default name is ses root entity name.xml in
		// project workspace)
		professional.writeSES();
		
	
	}
	
	
	
}	
