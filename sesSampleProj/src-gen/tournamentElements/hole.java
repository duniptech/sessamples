package tournamentElements;
import GenCol.*;
import genDevs.modeling.*;
import simView.*;
import modeling.*;
import ops.*;

public class hole extends ViewableDigraph{
	
	
	public hole(){
		this("tournamentElements.hole");
	}
	
	public hole(String name){
		super(name);
		
			
	}
	
	public static void main(String... args){
		EntitySES longer = new EntitySES("tournamentElements.longer");
		EntitySES shot = new EntitySES("tournamentElements.shot");
		EntitySES shorter = new EntitySES("tournamentElements.shorter");
		EntitySES shots = new EntitySES("tournamentElements.shots");
		
		EntitySES hole = new EntitySES("tournamentElements.hole");
		
		MultiSpec multihole = new MultiSpec("tournamentElements.multiHole", hole);	
		hole.addMultiSpecToEntity(multihole);
		hole.addEntityToMultiSpec("tournamentElements.multiHole",shots);
		
		MultiSpec multishots_shots = new MultiSpec("tournamentElements.multiShots", shots);	
		hole.addMultiSpecToEntity(multishots_shots);
		hole.addEntityToMultiSpec("tournamentElements.multiShots",shot);
		
		Spec shot_type_shot = new Spec("tournamentElements.shot_type", shot);
		hole.addSpecToEntity(shot_type_shot);
		hole.addEntityToSpec("tournamentElements.shot_type",longer);
		hole.addEntityToSpec("tournamentElements.shot_type",shorter);
		//show root in XML
		hole.info(hole.toXML());
		//show entity info for root
		hole.logEntityHierarchy();

		// write SES to file (default name is ses root entity name.xml in
		// project workspace)
		hole.writeSES();
		
	
	}
	
	
	
}	
