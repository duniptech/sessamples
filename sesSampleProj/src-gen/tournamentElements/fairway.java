package tournamentElements;
import GenCol.*;
import genDevs.modeling.*;
import simView.*;
import modeling.*;
import ops.*;

public class fairway extends ViewableAtomic{
	
	
	public fairway(){
		this("tournamentElements.fairway");
	}
	
	public fairway(String name){
		super(name);
		
			
	}
	
	public static void main(String... args){
		
		EntitySES fairway = new EntitySES("tournamentElements.fairway");
		//show root in XML
		fairway.info(fairway.toXML());
		//show entity info for root
		fairway.logEntityHierarchy();

		// write SES to file (default name is ses root entity name.xml in
		// project workspace)
		fairway.writeSES();
		
	
	}
	
	
	
}	
