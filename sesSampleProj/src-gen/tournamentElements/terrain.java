package tournamentElements;
import GenCol.*;
import genDevs.modeling.*;
import simView.*;
import modeling.*;
import ops.*;

public class terrain extends ViewableDigraph{
	
	
	public terrain(){
		this("tournamentElements.terrain");
	}
	
	public terrain(String name){
		super(name);
		
			
	}
	
	public static void main(String... args){
		EntitySES longer = new EntitySES("tournamentElements.longer");
		EntitySES fairway = new EntitySES("tournamentElements.fairway");
		EntitySES lie = new EntitySES("tournamentElements.lie");
		EntitySES shorter = new EntitySES("tournamentElements.shorter");
		EntitySES even = new EntitySES("tournamentElements.even");
		EntitySES away = new EntitySES("tournamentElements.away");
		EntitySES too = new EntitySES("tournamentElements.too");
		EntitySES rough = new EntitySES("tournamentElements.rough");
		EntitySES shot_length = new EntitySES("tournamentElements.shot_length");
		EntitySES slope = new EntitySES("tournamentElements.slope");
		
		EntitySES terrain = new EntitySES("tournamentElements.terrain");
		
		Aspect terrainstructure = new Aspect("tournamentElements.terrainStructure", terrain);
		terrain.addAspectToEntity(terrainstructure);
		terrain.addEntityToAspect("tournamentElements.terrainStructure",lie);	
		terrain.addEntityToAspect("tournamentElements.terrainStructure",shot_length);	
		terrain.addEntityToAspect("tournamentElements.terrainStructure",slope);	
		
		Spec liespec_lie = new Spec("tournamentElements.lieSpec", lie);
		terrain.addSpecToEntity(liespec_lie);
		terrain.addEntityToSpec("tournamentElements.lieSpec",fairway);
		terrain.addEntityToSpec("tournamentElements.lieSpec",rough);
		
		Spec shotlengthspec_shot_length = new Spec("tournamentElements.shotLengthSpec", shot_length);
		terrain.addSpecToEntity(shotlengthspec_shot_length);
		terrain.addEntityToSpec("tournamentElements.shotLengthSpec",longer);
		terrain.addEntityToSpec("tournamentElements.shotLengthSpec",shorter);
		
		Spec slopespec_slope = new Spec("tournamentElements.slopeSpec", slope);
		terrain.addSpecToEntity(slopespec_slope);
		terrain.addEntityToSpec("tournamentElements.slopeSpec",even);
		terrain.addEntityToSpec("tournamentElements.slopeSpec",away);
		terrain.addEntityToSpec("tournamentElements.slopeSpec",too);
		//show root in XML
		terrain.info(terrain.toXML());
		//show entity info for root
		terrain.logEntityHierarchy();

		// write SES to file (default name is ses root entity name.xml in
		// project workspace)
		terrain.writeSES();
		
	
	}
	
	
	
}	
