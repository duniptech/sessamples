package tournamentElements;
import GenCol.*;
import genDevs.modeling.*;
import simView.*;
import modeling.*;
import ops.*;

public class even extends ViewableAtomic{
	
	
	public even(){
		this("tournamentElements.even");
	}
	
	public even(String name){
		super(name);
		
			
	}
	
	public static void main(String... args){
		
		EntitySES even = new EntitySES("tournamentElements.even");
		//show root in XML
		even.info(even.toXML());
		//show entity info for root
		even.logEntityHierarchy();

		// write SES to file (default name is ses root entity name.xml in
		// project workspace)
		even.writeSES();
		
	
	}
	
	
	
}	
