package tournamentElements;
import GenCol.*;
import genDevs.modeling.*;
import simView.*;
import modeling.*;
import ops.*;

public class confident extends ViewableAtomic{
	
	
	public confident(){
		this("tournamentElements.confident");
	}
	
	public confident(String name){
		super(name);
		
			
	}
	
	public static void main(String... args){
		
		EntitySES confident = new EntitySES("tournamentElements.confident");
		//show root in XML
		confident.info(confident.toXML());
		//show entity info for root
		confident.logEntityHierarchy();

		// write SES to file (default name is ses root entity name.xml in
		// project workspace)
		confident.writeSES();
		
	
	}
	
	
	
}	
