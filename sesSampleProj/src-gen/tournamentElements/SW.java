package tournamentElements;
import GenCol.*;
import genDevs.modeling.*;
import simView.*;
import modeling.*;
import ops.*;

public class SW extends ViewableAtomic{
	
	
	public SW(){
		this("tournamentElements.SW");
	}
	
	public SW(String name){
		super(name);
		
			
	}
	
	public static void main(String... args){
		
		EntitySES sw = new EntitySES("tournamentElements.SW");
		//show root in XML
		sw.info(sw.toXML());
		//show entity info for root
		sw.logEntityHierarchy();

		// write SES to file (default name is ses root entity name.xml in
		// project workspace)
		sw.writeSES();
		
	
	}
	
	
	
}	
