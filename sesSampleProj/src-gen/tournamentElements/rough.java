package tournamentElements;
import GenCol.*;
import genDevs.modeling.*;
import simView.*;
import modeling.*;
import ops.*;

public class rough extends ViewableAtomic{
	
	
	public rough(){
		this("tournamentElements.rough");
	}
	
	public rough(String name){
		super(name);
		
			
	}
	
	public static void main(String... args){
		
		EntitySES rough = new EntitySES("tournamentElements.rough");
		//show root in XML
		rough.info(rough.toXML());
		//show entity info for root
		rough.logEntityHierarchy();

		// write SES to file (default name is ses root entity name.xml in
		// project workspace)
		rough.writeSES();
		
	
	}
	
	
	
}	
