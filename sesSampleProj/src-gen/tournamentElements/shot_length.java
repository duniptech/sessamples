package tournamentElements;
import GenCol.*;
import genDevs.modeling.*;
import simView.*;
import modeling.*;
import ops.*;

public class shot_length extends ViewableDigraph{
	
	
	public shot_length(){
		this("tournamentElements.shot_length");
	}
	
	public shot_length(String name){
		super(name);
		
			
	}
	
	public static void main(String... args){
		EntitySES longer = new EntitySES("tournamentElements.longer");
		EntitySES shorter = new EntitySES("tournamentElements.shorter");
		
		EntitySES shot_length = new EntitySES("tournamentElements.shot_length");
		
		Spec shotlengthspec = new Spec("tournamentElements.shotLengthSpec", shot_length);
		shot_length.addSpecToEntity(shotlengthspec);
		shot_length.addEntityToSpec("tournamentElements.shotLengthSpec",longer);
		shot_length.addEntityToSpec("tournamentElements.shotLengthSpec",shorter);
		//show root in XML
		shot_length.info(shot_length.toXML());
		//show entity info for root
		shot_length.logEntityHierarchy();

		// write SES to file (default name is ses root entity name.xml in
		// project workspace)
		shot_length.writeSES();
		
	
	}
	
	
	
}	
