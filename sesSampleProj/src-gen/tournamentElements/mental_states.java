package tournamentElements;
import GenCol.*;
import genDevs.modeling.*;
import simView.*;
import modeling.*;
import ops.*;

public class mental_states extends ViewableDigraph{
	
	
	public mental_states(){
		this("tournamentElements.mental_states");
	}
	
	public mental_states(String name){
		super(name);
		
			
	}
	
	public static void main(String... args){
		EntitySES mental_state = new EntitySES("tournamentElements.mental_state");
		EntitySES anxious = new EntitySES("tournamentElements.anxious");
		EntitySES confident = new EntitySES("tournamentElements.confident");
		
		EntitySES mental_states = new EntitySES("tournamentElements.mental_states");
		
		MultiSpec multimentalstates = new MultiSpec("tournamentElements.multiMentalStates", mental_states);	
		mental_states.addMultiSpecToEntity(multimentalstates);
		mental_states.addEntityToMultiSpec("tournamentElements.multiMentalStates",mental_state);
		
		Spec mentalstatespec_mental_state = new Spec("tournamentElements.mentalStateSpec", mental_state);
		mental_states.addSpecToEntity(mentalstatespec_mental_state);
		mental_states.addEntityToSpec("tournamentElements.mentalStateSpec",anxious);
		mental_states.addEntityToSpec("tournamentElements.mentalStateSpec",confident);
		//show root in XML
		mental_states.info(mental_states.toXML());
		//show entity info for root
		mental_states.logEntityHierarchy();

		// write SES to file (default name is ses root entity name.xml in
		// project workspace)
		mental_states.writeSES();
		
	
	}
	
	
	
}	
