package tournamentElements;
import GenCol.*;
import genDevs.modeling.*;
import simView.*;
import modeling.*;
import ops.*;

public class longer extends ViewableAtomic{
	
	
	public longer(){
		this("tournamentElements.longer");
	}
	
	public longer(String name){
		super(name);
		
			
	}
	
	public static void main(String... args){
		
		EntitySES longer = new EntitySES("tournamentElements.longer");
		//show root in XML
		longer.info(longer.toXML());
		//show entity info for root
		longer.logEntityHierarchy();

		// write SES to file (default name is ses root entity name.xml in
		// project workspace)
		longer.writeSES();
		
	
	}
	
	
	
}	
