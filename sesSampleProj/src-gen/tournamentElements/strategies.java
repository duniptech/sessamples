package tournamentElements;
import GenCol.*;
import genDevs.modeling.*;
import simView.*;
import modeling.*;
import ops.*;

public class strategies extends ViewableDigraph{
	
	
	public strategies(){
		this("tournamentElements.strategies");
	}
	
	public strategies(String name){
		super(name);
		
			
	}
	
	public static void main(String... args){
		EntitySES draw = new EntitySES("tournamentElements.draw");
		EntitySES backspin = new EntitySES("tournamentElements.backspin");
		EntitySES strategy = new EntitySES("tournamentElements.strategy");
		EntitySES punch = new EntitySES("tournamentElements.punch");
		
		EntitySES strategies = new EntitySES("tournamentElements.strategies");
		
		MultiSpec multistrategy = new MultiSpec("tournamentElements.multiStrategy", strategies);	
		strategies.addMultiSpecToEntity(multistrategy);
		strategies.addEntityToMultiSpec("tournamentElements.multiStrategy",strategy);
		
		Spec strategyspec_strategy = new Spec("tournamentElements.strategySpec", strategy);
		strategies.addSpecToEntity(strategyspec_strategy);
		strategies.addEntityToSpec("tournamentElements.strategySpec",draw);
		strategies.addEntityToSpec("tournamentElements.strategySpec",backspin);
		strategies.addEntityToSpec("tournamentElements.strategySpec",punch);
		//show root in XML
		strategies.info(strategies.toXML());
		//show entity info for root
		strategies.logEntityHierarchy();

		// write SES to file (default name is ses root entity name.xml in
		// project workspace)
		strategies.writeSES();
		
	
	}
	
	
	
}	
