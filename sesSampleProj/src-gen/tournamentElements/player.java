package tournamentElements;
import GenCol.*;
import genDevs.modeling.*;
import simView.*;
import modeling.*;
import ops.*;

public class player extends ViewableDigraph{
	
	
	public player(){
		this("tournamentElements.player");
	}
	
	public player(String name){
		super(name);
		
			
	}
	
	public static void main(String... args){
		EntitySES onewood = new EntitySES("tournamentElements.OneWood");
		EntitySES draw = new EntitySES("tournamentElements.draw");
		EntitySES mental_states = new EntitySES("tournamentElements.mental_states");
		EntitySES sw = new EntitySES("tournamentElements.SW");
		EntitySES anxious = new EntitySES("tournamentElements.anxious");
		EntitySES clubs = new EntitySES("tournamentElements.clubs");
		EntitySES confident = new EntitySES("tournamentElements.confident");
		EntitySES punch = new EntitySES("tournamentElements.punch");
		EntitySES caddy = new EntitySES("tournamentElements.caddy");
		EntitySES club = new EntitySES("tournamentElements.club");
		EntitySES backspin = new EntitySES("tournamentElements.backspin");
		EntitySES professional = new EntitySES("tournamentElements.professional");
		EntitySES mental_state = new EntitySES("tournamentElements.mental_state");
		EntitySES amateur = new EntitySES("tournamentElements.amateur");
		EntitySES strategies = new EntitySES("tournamentElements.strategies");
		EntitySES strategy = new EntitySES("tournamentElements.strategy");
		
		EntitySES player = new EntitySES("tournamentElements.player");
		
		Spec playerspec = new Spec("tournamentElements.playerSpec", player);
		player.addSpecToEntity(playerspec);
		player.addEntityToSpec("tournamentElements.playerSpec",professional);
		player.addEntityToSpec("tournamentElements.playerSpec",amateur);
		
		Aspect playerstructure = new Aspect("tournamentElements.playerStructure", player);
		player.addAspectToEntity(playerstructure);
		player.addEntityToAspect("tournamentElements.playerStructure",caddy);	
		player.addEntityToAspect("tournamentElements.playerStructure",mental_states);	
		player.addEntityToAspect("tournamentElements.playerStructure",strategies);	
		
		Aspect caddystructure_caddy = new Aspect("tournamentElements.caddyStructure", caddy);
		player.addAspectToEntity(caddystructure_caddy);
		player.addEntityToAspect("tournamentElements.caddyStructure",clubs);	
		
		MultiSpec multimentalstates_mental_states = new MultiSpec("tournamentElements.multiMentalStates", mental_states);	
		player.addMultiSpecToEntity(multimentalstates_mental_states);
		player.addEntityToMultiSpec("tournamentElements.multiMentalStates",mental_state);
		
		MultiSpec multiclubs_clubs = new MultiSpec("tournamentElements.multiClubs", clubs);	
		player.addMultiSpecToEntity(multiclubs_clubs);
		player.addEntityToMultiSpec("tournamentElements.multiClubs",club);
		
		MultiSpec multistrategy_strategies = new MultiSpec("tournamentElements.multiStrategy", strategies);	
		player.addMultiSpecToEntity(multistrategy_strategies);
		player.addEntityToMultiSpec("tournamentElements.multiStrategy",strategy);
		
		Spec clubspec_club = new Spec("tournamentElements.clubSpec", club);
		player.addSpecToEntity(clubspec_club);
		player.addEntityToSpec("tournamentElements.clubSpec",onewood);
		player.addEntityToSpec("tournamentElements.clubSpec",sw);
		
		Spec mentalstatespec_mental_state = new Spec("tournamentElements.mentalStateSpec", mental_state);
		player.addSpecToEntity(mentalstatespec_mental_state);
		player.addEntityToSpec("tournamentElements.mentalStateSpec",anxious);
		player.addEntityToSpec("tournamentElements.mentalStateSpec",confident);
		
		Spec strategyspec_strategy = new Spec("tournamentElements.strategySpec", strategy);
		player.addSpecToEntity(strategyspec_strategy);
		player.addEntityToSpec("tournamentElements.strategySpec",draw);
		player.addEntityToSpec("tournamentElements.strategySpec",backspin);
		player.addEntityToSpec("tournamentElements.strategySpec",punch);
		//show root in XML
		player.info(player.toXML());
		//show entity info for root
		player.logEntityHierarchy();

		// write SES to file (default name is ses root entity name.xml in
		// project workspace)
		player.writeSES();
		
	
	}
	
	
	
}	
