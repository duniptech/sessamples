package tournamentElements;
import GenCol.*;
import genDevs.modeling.*;
import simView.*;
import modeling.*;
import ops.*;

public class anxious extends ViewableAtomic{
	
	
	public anxious(){
		this("tournamentElements.anxious");
	}
	
	public anxious(String name){
		super(name);
		
			
	}
	
	public static void main(String... args){
		
		EntitySES anxious = new EntitySES("tournamentElements.anxious");
		//show root in XML
		anxious.info(anxious.toXML());
		//show entity info for root
		anxious.logEntityHierarchy();

		// write SES to file (default name is ses root entity name.xml in
		// project workspace)
		anxious.writeSES();
		
	
	}
	
	
	
}	
