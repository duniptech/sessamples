package tournamentElements;
import GenCol.*;
import genDevs.modeling.*;
import simView.*;
import modeling.*;
import ops.*;

public class caddy extends ViewableDigraph{
	
	
	public caddy(){
		this("tournamentElements.caddy");
	}
	
	public caddy(String name){
		super(name);
		
			
	}
	
	public static void main(String... args){
		EntitySES club = new EntitySES("tournamentElements.club");
		EntitySES onewood = new EntitySES("tournamentElements.OneWood");
		EntitySES sw = new EntitySES("tournamentElements.SW");
		EntitySES clubs = new EntitySES("tournamentElements.clubs");
		
		EntitySES caddy = new EntitySES("tournamentElements.caddy");
		
		Aspect caddystructure = new Aspect("tournamentElements.caddyStructure", caddy);
		caddy.addAspectToEntity(caddystructure);
		caddy.addEntityToAspect("tournamentElements.caddyStructure",clubs);	
		
		MultiSpec multiclubs_clubs = new MultiSpec("tournamentElements.multiClubs", clubs);	
		caddy.addMultiSpecToEntity(multiclubs_clubs);
		caddy.addEntityToMultiSpec("tournamentElements.multiClubs",club);
		
		Spec clubspec_club = new Spec("tournamentElements.clubSpec", club);
		caddy.addSpecToEntity(clubspec_club);
		caddy.addEntityToSpec("tournamentElements.clubSpec",onewood);
		caddy.addEntityToSpec("tournamentElements.clubSpec",sw);
		//show root in XML
		caddy.info(caddy.toXML());
		//show entity info for root
		caddy.logEntityHierarchy();

		// write SES to file (default name is ses root entity name.xml in
		// project workspace)
		caddy.writeSES();
		
	
	}
	
	
	
}	
