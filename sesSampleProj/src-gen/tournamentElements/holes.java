package tournamentElements;
import GenCol.*;
import genDevs.modeling.*;
import simView.*;
import modeling.*;
import ops.*;

public class holes extends ViewableAtomic{
	
	
	public holes(){
		this("tournamentElements.holes");
	}
	
	public holes(String name){
		super(name);
		
			
	}
	
	public static void main(String... args){
		
		EntitySES holes = new EntitySES("tournamentElements.holes");
		//show root in XML
		holes.info(holes.toXML());
		//show entity info for root
		holes.logEntityHierarchy();

		// write SES to file (default name is ses root entity name.xml in
		// project workspace)
		holes.writeSES();
		
	
	}
	
	
	
}	
