package tournamentElements;
import GenCol.*;
import genDevs.modeling.*;
import simView.*;
import modeling.*;
import ops.*;

public class lie extends ViewableDigraph{
	
	
	public lie(){
		this("tournamentElements.lie");
	}
	
	public lie(String name){
		super(name);
		
			
	}
	
	public static void main(String... args){
		EntitySES fairway = new EntitySES("tournamentElements.fairway");
		EntitySES rough = new EntitySES("tournamentElements.rough");
		
		EntitySES lie = new EntitySES("tournamentElements.lie");
		
		Spec liespec = new Spec("tournamentElements.lieSpec", lie);
		lie.addSpecToEntity(liespec);
		lie.addEntityToSpec("tournamentElements.lieSpec",fairway);
		lie.addEntityToSpec("tournamentElements.lieSpec",rough);
		//show root in XML
		lie.info(lie.toXML());
		//show entity info for root
		lie.logEntityHierarchy();

		// write SES to file (default name is ses root entity name.xml in
		// project workspace)
		lie.writeSES();
		
	
	}
	
	
	
}	
