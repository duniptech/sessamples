package tournamentElements;
import GenCol.*;
import genDevs.modeling.*;
import simView.*;
import modeling.*;
import ops.*;

public class shots extends ViewableDigraph{
	
	
	public shots(){
		this("tournamentElements.shots");
	}
	
	public shots(String name){
		super(name);
		
			
	}
	
	public static void main(String... args){
		EntitySES longer = new EntitySES("tournamentElements.longer");
		EntitySES shot = new EntitySES("tournamentElements.shot");
		EntitySES shorter = new EntitySES("tournamentElements.shorter");
		
		EntitySES shots = new EntitySES("tournamentElements.shots");
		
		MultiSpec multishots = new MultiSpec("tournamentElements.multiShots", shots);	
		shots.addMultiSpecToEntity(multishots);
		shots.addEntityToMultiSpec("tournamentElements.multiShots",shot);
		
		Spec shot_type_shot = new Spec("tournamentElements.shot_type", shot);
		shots.addSpecToEntity(shot_type_shot);
		shots.addEntityToSpec("tournamentElements.shot_type",longer);
		shots.addEntityToSpec("tournamentElements.shot_type",shorter);
		//show root in XML
		shots.info(shots.toXML());
		//show entity info for root
		shots.logEntityHierarchy();

		// write SES to file (default name is ses root entity name.xml in
		// project workspace)
		shots.writeSES();
		
	
	}
	
	
	
}	
