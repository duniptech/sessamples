package tournamentElements;
import GenCol.*;
import genDevs.modeling.*;
import simView.*;
import modeling.*;
import ops.*;

public class away extends ViewableAtomic{
	
	
	public away(){
		this("tournamentElements.away");
	}
	
	public away(String name){
		super(name);
		
			
	}
	
	public static void main(String... args){
		
		EntitySES away = new EntitySES("tournamentElements.away");
		//show root in XML
		away.info(away.toXML());
		//show entity info for root
		away.logEntityHierarchy();

		// write SES to file (default name is ses root entity name.xml in
		// project workspace)
		away.writeSES();
		
	
	}
	
	
	
}	
