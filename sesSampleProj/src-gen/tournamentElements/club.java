package tournamentElements;
import GenCol.*;
import genDevs.modeling.*;
import simView.*;
import modeling.*;
import ops.*;

public class club extends ViewableDigraph{
	
	
	public club(){
		this("tournamentElements.club");
	}
	
	public club(String name){
		super(name);
		
			
	}
	
	public static void main(String... args){
		EntitySES onewood = new EntitySES("tournamentElements.OneWood");
		EntitySES sw = new EntitySES("tournamentElements.SW");
		
		EntitySES club = new EntitySES("tournamentElements.club");
		
		Spec clubspec = new Spec("tournamentElements.clubSpec", club);
		club.addSpecToEntity(clubspec);
		club.addEntityToSpec("tournamentElements.clubSpec",onewood);
		club.addEntityToSpec("tournamentElements.clubSpec",sw);
		//show root in XML
		club.info(club.toXML());
		//show entity info for root
		club.logEntityHierarchy();

		// write SES to file (default name is ses root entity name.xml in
		// project workspace)
		club.writeSES();
		
	
	}
	
	
	
}	
