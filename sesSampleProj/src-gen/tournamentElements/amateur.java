package tournamentElements;
import GenCol.*;
import genDevs.modeling.*;
import simView.*;
import modeling.*;
import ops.*;

public class amateur extends ViewableAtomic{
	
	
	public amateur(){
		this("tournamentElements.amateur");
	}
	
	public amateur(String name){
		super(name);
		
			
	}
	
	public static void main(String... args){
		
		EntitySES amateur = new EntitySES("tournamentElements.amateur");
		//show root in XML
		amateur.info(amateur.toXML());
		//show entity info for root
		amateur.logEntityHierarchy();

		// write SES to file (default name is ses root entity name.xml in
		// project workspace)
		amateur.writeSES();
		
	
	}
	
	
	
}	
