package tournamentElements;
import GenCol.*;
import genDevs.modeling.*;
import simView.*;
import modeling.*;
import ops.*;

public class backspin extends ViewableAtomic{
	
	
	public backspin(){
		this("tournamentElements.backspin");
	}
	
	public backspin(String name){
		super(name);
		
			
	}
	
	public static void main(String... args){
		
		EntitySES backspin = new EntitySES("tournamentElements.backspin");
		//show root in XML
		backspin.info(backspin.toXML());
		//show entity info for root
		backspin.logEntityHierarchy();

		// write SES to file (default name is ses root entity name.xml in
		// project workspace)
		backspin.writeSES();
		
	
	}
	
	
	
}	
