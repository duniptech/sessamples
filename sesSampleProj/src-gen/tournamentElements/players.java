package tournamentElements;
import GenCol.*;
import genDevs.modeling.*;
import simView.*;
import modeling.*;
import ops.*;

public class players extends ViewableDigraph{
	
	
	public players(){
		this("tournamentElements.players");
	}
	
	public players(String name){
		super(name);
		
			
	}
	
	public static void main(String... args){
		EntitySES onewood = new EntitySES("tournamentElements.OneWood");
		EntitySES draw = new EntitySES("tournamentElements.draw");
		EntitySES mental_states = new EntitySES("tournamentElements.mental_states");
		EntitySES player = new EntitySES("tournamentElements.player");
		EntitySES sw = new EntitySES("tournamentElements.SW");
		EntitySES anxious = new EntitySES("tournamentElements.anxious");
		EntitySES clubs = new EntitySES("tournamentElements.clubs");
		EntitySES confident = new EntitySES("tournamentElements.confident");
		EntitySES punch = new EntitySES("tournamentElements.punch");
		EntitySES caddy = new EntitySES("tournamentElements.caddy");
		EntitySES club = new EntitySES("tournamentElements.club");
		EntitySES backspin = new EntitySES("tournamentElements.backspin");
		EntitySES professional = new EntitySES("tournamentElements.professional");
		EntitySES mental_state = new EntitySES("tournamentElements.mental_state");
		EntitySES amateur = new EntitySES("tournamentElements.amateur");
		EntitySES strategies = new EntitySES("tournamentElements.strategies");
		EntitySES strategy = new EntitySES("tournamentElements.strategy");
		
		EntitySES players = new EntitySES("tournamentElements.players");
		
		MultiSpec multispecplayer = new MultiSpec("tournamentElements.multiSpecPlayer", players);	
		players.addMultiSpecToEntity(multispecplayer);
		players.addEntityToMultiSpec("tournamentElements.multiSpecPlayer",player);
		
		Aspect playerstructure_player = new Aspect("tournamentElements.playerStructure", player);
		players.addAspectToEntity(playerstructure_player);
		players.addEntityToAspect("tournamentElements.playerStructure",caddy);	
		players.addEntityToAspect("tournamentElements.playerStructure",mental_states);	
		players.addEntityToAspect("tournamentElements.playerStructure",strategies);	
		
		Aspect caddystructure_caddy = new Aspect("tournamentElements.caddyStructure", caddy);
		players.addAspectToEntity(caddystructure_caddy);
		players.addEntityToAspect("tournamentElements.caddyStructure",clubs);	
		
		MultiSpec multimentalstates_mental_states = new MultiSpec("tournamentElements.multiMentalStates", mental_states);	
		players.addMultiSpecToEntity(multimentalstates_mental_states);
		players.addEntityToMultiSpec("tournamentElements.multiMentalStates",mental_state);
		
		MultiSpec multiclubs_clubs = new MultiSpec("tournamentElements.multiClubs", clubs);	
		players.addMultiSpecToEntity(multiclubs_clubs);
		players.addEntityToMultiSpec("tournamentElements.multiClubs",club);
		
		MultiSpec multistrategy_strategies = new MultiSpec("tournamentElements.multiStrategy", strategies);	
		players.addMultiSpecToEntity(multistrategy_strategies);
		players.addEntityToMultiSpec("tournamentElements.multiStrategy",strategy);
		
		Spec playerspec_player = new Spec("tournamentElements.playerSpec", player);
		players.addSpecToEntity(playerspec_player);
		players.addEntityToSpec("tournamentElements.playerSpec",professional);
		players.addEntityToSpec("tournamentElements.playerSpec",amateur);
		
		Spec clubspec_club = new Spec("tournamentElements.clubSpec", club);
		players.addSpecToEntity(clubspec_club);
		players.addEntityToSpec("tournamentElements.clubSpec",onewood);
		players.addEntityToSpec("tournamentElements.clubSpec",sw);
		
		Spec mentalstatespec_mental_state = new Spec("tournamentElements.mentalStateSpec", mental_state);
		players.addSpecToEntity(mentalstatespec_mental_state);
		players.addEntityToSpec("tournamentElements.mentalStateSpec",anxious);
		players.addEntityToSpec("tournamentElements.mentalStateSpec",confident);
		
		Spec strategyspec_strategy = new Spec("tournamentElements.strategySpec", strategy);
		players.addSpecToEntity(strategyspec_strategy);
		players.addEntityToSpec("tournamentElements.strategySpec",draw);
		players.addEntityToSpec("tournamentElements.strategySpec",backspin);
		players.addEntityToSpec("tournamentElements.strategySpec",punch);
		//show root in XML
		players.info(players.toXML());
		//show entity info for root
		players.logEntityHierarchy();

		// write SES to file (default name is ses root entity name.xml in
		// project workspace)
		players.writeSES();
		
	
	}
	
	
	
}	
