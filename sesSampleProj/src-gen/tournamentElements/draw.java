package tournamentElements;
import GenCol.*;
import genDevs.modeling.*;
import simView.*;
import modeling.*;
import ops.*;

public class draw extends ViewableAtomic{
	
	
	public draw(){
		this("tournamentElements.draw");
	}
	
	public draw(String name){
		super(name);
		
			
	}
	
	public static void main(String... args){
		
		EntitySES draw = new EntitySES("tournamentElements.draw");
		//show root in XML
		draw.info(draw.toXML());
		//show entity info for root
		draw.logEntityHierarchy();

		// write SES to file (default name is ses root entity name.xml in
		// project workspace)
		draw.writeSES();
		
	
	}
	
	
	
}	
