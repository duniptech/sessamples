package tournamentElements;
import GenCol.*;
import genDevs.modeling.*;
import simView.*;
import modeling.*;
import ops.*;

public class shorter extends ViewableAtomic{
	
	public int length;
	
	public shorter(){
		this("tournamentElements.shorter");
	}
	
	public shorter(String name){
		super(name);
		
			
	}
	
	public static void main(String... args){
		
		EntitySES shorter = new EntitySES("tournamentElements.shorter");
		//show root in XML
		shorter.info(shorter.toXML());
		//show entity info for root
		shorter.logEntityHierarchy();

		// write SES to file (default name is ses root entity name.xml in
		// project workspace)
		shorter.writeSES();
		
	
	}
	
	
	
}	
