package tournamentElements;
import GenCol.*;
import genDevs.modeling.*;
import simView.*;
import modeling.*;
import ops.*;

public class too extends ViewableAtomic{
	
	
	public too(){
		this("tournamentElements.too");
	}
	
	public too(String name){
		super(name);
		
			
	}
	
	public static void main(String... args){
		
		EntitySES too = new EntitySES("tournamentElements.too");
		//show root in XML
		too.info(too.toXML());
		//show entity info for root
		too.logEntityHierarchy();

		// write SES to file (default name is ses root entity name.xml in
		// project workspace)
		too.writeSES();
		
	
	}
	
	
	
}	
