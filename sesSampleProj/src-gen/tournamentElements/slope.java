package tournamentElements;
import GenCol.*;
import genDevs.modeling.*;
import simView.*;
import modeling.*;
import ops.*;

public class slope extends ViewableDigraph{
	
	
	public slope(){
		this("tournamentElements.slope");
	}
	
	public slope(String name){
		super(name);
		
			
	}
	
	public static void main(String... args){
		EntitySES even = new EntitySES("tournamentElements.even");
		EntitySES away = new EntitySES("tournamentElements.away");
		EntitySES too = new EntitySES("tournamentElements.too");
		
		EntitySES slope = new EntitySES("tournamentElements.slope");
		
		Spec slopespec = new Spec("tournamentElements.slopeSpec", slope);
		slope.addSpecToEntity(slopespec);
		slope.addEntityToSpec("tournamentElements.slopeSpec",even);
		slope.addEntityToSpec("tournamentElements.slopeSpec",away);
		slope.addEntityToSpec("tournamentElements.slopeSpec",too);
		//show root in XML
		slope.info(slope.toXML());
		//show entity info for root
		slope.logEntityHierarchy();

		// write SES to file (default name is ses root entity name.xml in
		// project workspace)
		slope.writeSES();
		
	
	}
	
	
	
}	
