package tournamentElements;
import GenCol.*;
import genDevs.modeling.*;
import simView.*;
import modeling.*;
import ops.*;

public class rounds extends ViewableDigraph{
	
	
	public rounds(){
		this("tournamentElements.rounds");
	}
	
	public rounds(String name){
		super(name);
		
			
	}
	
	public static void main(String... args){
		EntitySES round = new EntitySES("tournamentElements.round");
		EntitySES longer = new EntitySES("tournamentElements.longer");
		EntitySES shot = new EntitySES("tournamentElements.shot");
		EntitySES hole = new EntitySES("tournamentElements.hole");
		EntitySES shorter = new EntitySES("tournamentElements.shorter");
		EntitySES shots = new EntitySES("tournamentElements.shots");
		
		EntitySES rounds = new EntitySES("tournamentElements.rounds");
		
		MultiSpec multirounds = new MultiSpec("tournamentElements.multiRounds", rounds);	
		rounds.addMultiSpecToEntity(multirounds);
		rounds.addEntityToMultiSpec("tournamentElements.multiRounds",round);
		
		MultiSpec multiround_round = new MultiSpec("tournamentElements.multiRound", round);	
		rounds.addMultiSpecToEntity(multiround_round);
		rounds.addEntityToMultiSpec("tournamentElements.multiRound",hole);
		
		MultiSpec multihole_hole = new MultiSpec("tournamentElements.multiHole", hole);	
		rounds.addMultiSpecToEntity(multihole_hole);
		rounds.addEntityToMultiSpec("tournamentElements.multiHole",shots);
		
		MultiSpec multishots_shots = new MultiSpec("tournamentElements.multiShots", shots);	
		rounds.addMultiSpecToEntity(multishots_shots);
		rounds.addEntityToMultiSpec("tournamentElements.multiShots",shot);
		
		Spec shot_type_shot = new Spec("tournamentElements.shot_type", shot);
		rounds.addSpecToEntity(shot_type_shot);
		rounds.addEntityToSpec("tournamentElements.shot_type",longer);
		rounds.addEntityToSpec("tournamentElements.shot_type",shorter);
		//show root in XML
		rounds.info(rounds.toXML());
		//show entity info for root
		rounds.logEntityHierarchy();

		// write SES to file (default name is ses root entity name.xml in
		// project workspace)
		rounds.writeSES();
		
	
	}
	
	
	
}	
