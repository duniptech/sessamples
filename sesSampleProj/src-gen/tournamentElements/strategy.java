package tournamentElements;
import GenCol.*;
import genDevs.modeling.*;
import simView.*;
import modeling.*;
import ops.*;

public class strategy extends ViewableDigraph{
	
	
	public strategy(){
		this("tournamentElements.strategy");
	}
	
	public strategy(String name){
		super(name);
		
			
	}
	
	public static void main(String... args){
		EntitySES draw = new EntitySES("tournamentElements.draw");
		EntitySES backspin = new EntitySES("tournamentElements.backspin");
		EntitySES punch = new EntitySES("tournamentElements.punch");
		
		EntitySES strategy = new EntitySES("tournamentElements.strategy");
		
		Spec strategyspec = new Spec("tournamentElements.strategySpec", strategy);
		strategy.addSpecToEntity(strategyspec);
		strategy.addEntityToSpec("tournamentElements.strategySpec",draw);
		strategy.addEntityToSpec("tournamentElements.strategySpec",backspin);
		strategy.addEntityToSpec("tournamentElements.strategySpec",punch);
		//show root in XML
		strategy.info(strategy.toXML());
		//show entity info for root
		strategy.logEntityHierarchy();

		// write SES to file (default name is ses root entity name.xml in
		// project workspace)
		strategy.writeSES();
		
	
	}
	
	
	
}	
