package tournamentElements;
import GenCol.*;
import genDevs.modeling.*;
import simView.*;
import modeling.*;
import ops.*;

public class shot extends ViewableDigraph{
	
	public int range;
	public double value;
	
	public shot(){
		this("tournamentElements.shot");
	}
	
	public shot(String name){
		super(name);
		
			
	}
	
	public static void main(String... args){
		EntitySES longer = new EntitySES("tournamentElements.longer");
		EntitySES shorter = new EntitySES("tournamentElements.shorter");
		
		EntitySES shot = new EntitySES("tournamentElements.shot");
		
		Spec shot_type = new Spec("tournamentElements.shot_type", shot);
		shot.addSpecToEntity(shot_type);
		shot.addEntityToSpec("tournamentElements.shot_type",longer);
		shot.addEntityToSpec("tournamentElements.shot_type",shorter);
		//show root in XML
		shot.info(shot.toXML());
		//show entity info for root
		shot.logEntityHierarchy();

		// write SES to file (default name is ses root entity name.xml in
		// project workspace)
		shot.writeSES();
		
	
	}
	
	
	
}	
