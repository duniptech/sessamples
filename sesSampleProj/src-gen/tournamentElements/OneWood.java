package tournamentElements;
import GenCol.*;
import genDevs.modeling.*;
import simView.*;
import modeling.*;
import ops.*;

public class OneWood extends ViewableAtomic{
	
	
	public OneWood(){
		this("tournamentElements.OneWood");
	}
	
	public OneWood(String name){
		super(name);
		
			
	}
	
	public static void main(String... args){
		
		EntitySES onewood = new EntitySES("tournamentElements.OneWood");
		//show root in XML
		onewood.info(onewood.toXML());
		//show entity info for root
		onewood.logEntityHierarchy();

		// write SES to file (default name is ses root entity name.xml in
		// project workspace)
		onewood.writeSES();
		
	
	}
	
	
	
}	
