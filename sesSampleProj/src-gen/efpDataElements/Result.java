package efpDataElements;
import GenCol.*;

public class Result extends entity{
	
	public double generated;
	public double solved;
	public efpDataElements.Job lastSolved;
	public double throughput;
	
	public Result(){
		super("efpDataElements.Result");
	}
	
}
