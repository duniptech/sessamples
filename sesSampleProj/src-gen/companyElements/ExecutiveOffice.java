package companyElements;
import GenCol.*;
import genDevs.modeling.*;
import simView.*;
import modeling.*;
import ops.*;

public class ExecutiveOffice extends ViewableDigraph{
	
	
	public ExecutiveOffice(){
		this("companyElements.ExecutiveOffice");
	}
	
	public ExecutiveOffice(String name){
		super(name);
		
			
	}
	
	public static void main(String... args){
		EntitySES secondofficer = new EntitySES("companyElements.SecondOfficer");
		EntitySES firstofficer = new EntitySES("companyElements.FirstOfficer");
		
		EntitySES executiveoffice = new EntitySES("companyElements.ExecutiveOffice");
		
		Spec persondec = new Spec("companyElements.PersonDec", executiveoffice);
		executiveoffice.addSpecToEntity(persondec);
		executiveoffice.addEntityToSpec("companyElements.PersonDec",secondofficer);
		executiveoffice.addEntityToSpec("companyElements.PersonDec",firstofficer);
		//show root in XML
		executiveoffice.info(executiveoffice.toXML());
		//show entity info for root
		executiveoffice.logEntityHierarchy();

		// write SES to file (default name is ses root entity name.xml in
		// project workspace)
		executiveoffice.writeSES();
		
	
	}
	
	
	
}	
