package companyElements;
import GenCol.*;
import genDevs.modeling.*;
import simView.*;
import modeling.*;
import ops.*;

public class SalesDepartment extends ViewableDigraph{
	
	
	public SalesDepartment(){
		this("companyElements.SalesDepartment");
	}
	
	public SalesDepartment(String name){
		super(name);
		
			
	}
	
	public static void main(String... args){
		EntitySES secondofficer = new EntitySES("companyElements.SecondOfficer");
		EntitySES executiveoffice = new EntitySES("companyElements.ExecutiveOffice");
		EntitySES firstofficer = new EntitySES("companyElements.FirstOfficer");
		
		EntitySES salesdepartment = new EntitySES("companyElements.SalesDepartment");
		
		Aspect bottomorgdec = new Aspect("companyElements.BottomOrgDec", salesdepartment);
		salesdepartment.addAspectToEntity(bottomorgdec);
		salesdepartment.addEntityToAspect("companyElements.BottomOrgDec",executiveoffice);	
		
		Spec persondec_executiveoffice = new Spec("companyElements.PersonDec", executiveoffice);
		salesdepartment.addSpecToEntity(persondec_executiveoffice);
		salesdepartment.addEntityToSpec("companyElements.PersonDec",secondofficer);
		salesdepartment.addEntityToSpec("companyElements.PersonDec",firstofficer);
		//show root in XML
		salesdepartment.info(salesdepartment.toXML());
		//show entity info for root
		salesdepartment.logEntityHierarchy();

		// write SES to file (default name is ses root entity name.xml in
		// project workspace)
		salesdepartment.writeSES();
		
	
	}
	
	
	
}	
