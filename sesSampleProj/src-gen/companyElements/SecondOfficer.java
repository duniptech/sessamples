package companyElements;
import GenCol.*;
import genDevs.modeling.*;
import simView.*;
import modeling.*;
import ops.*;

public class SecondOfficer extends ViewableAtomic{
	
	
	public SecondOfficer(){
		this("companyElements.SecondOfficer");
	}
	
	public SecondOfficer(String name){
		super(name);
		
			
	}
	
	public static void main(String... args){
		
		EntitySES secondofficer = new EntitySES("companyElements.SecondOfficer");
		//show root in XML
		secondofficer.info(secondofficer.toXML());
		//show entity info for root
		secondofficer.logEntityHierarchy();

		// write SES to file (default name is ses root entity name.xml in
		// project workspace)
		secondofficer.writeSES();
		
	
	}
	
	
	
}	
