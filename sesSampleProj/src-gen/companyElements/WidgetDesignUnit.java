package companyElements;
import GenCol.*;
import genDevs.modeling.*;
import simView.*;
import modeling.*;
import ops.*;

public class WidgetDesignUnit extends ViewableDigraph{
	
	
	public WidgetDesignUnit(){
		this("companyElements.WidgetDesignUnit");
	}
	
	public WidgetDesignUnit(String name){
		super(name);
		
			
	}
	
	public static void main(String... args){
		EntitySES secondofficer = new EntitySES("companyElements.SecondOfficer");
		EntitySES executiveoffice = new EntitySES("companyElements.ExecutiveOffice");
		EntitySES firstofficer = new EntitySES("companyElements.FirstOfficer");
		
		EntitySES widgetdesignunit = new EntitySES("companyElements.WidgetDesignUnit");
		
		Aspect bottomorgdec = new Aspect("companyElements.BottomOrgDec", widgetdesignunit);
		widgetdesignunit.addAspectToEntity(bottomorgdec);
		widgetdesignunit.addEntityToAspect("companyElements.BottomOrgDec",executiveoffice);	
		
		Spec persondec_executiveoffice = new Spec("companyElements.PersonDec", executiveoffice);
		widgetdesignunit.addSpecToEntity(persondec_executiveoffice);
		widgetdesignunit.addEntityToSpec("companyElements.PersonDec",secondofficer);
		widgetdesignunit.addEntityToSpec("companyElements.PersonDec",firstofficer);
		//show root in XML
		widgetdesignunit.info(widgetdesignunit.toXML());
		//show entity info for root
		widgetdesignunit.logEntityHierarchy();

		// write SES to file (default name is ses root entity name.xml in
		// project workspace)
		widgetdesignunit.writeSES();
		
	
	}
	
	
	
}	
