package companyElements;
import GenCol.*;
import genDevs.modeling.*;
import simView.*;
import modeling.*;
import ops.*;

public class ProductionDepartment extends ViewableDigraph{
	
	
	public ProductionDepartment(){
		this("companyElements.ProductionDepartment");
	}
	
	public ProductionDepartment(String name){
		super(name);
		
			
	}
	
	public static void main(String... args){
		EntitySES widgetdesignunit = new EntitySES("companyElements.WidgetDesignUnit");
		EntitySES secondofficer = new EntitySES("companyElements.SecondOfficer");
		EntitySES executiveoffice = new EntitySES("companyElements.ExecutiveOffice");
		EntitySES widgetmakingunit = new EntitySES("companyElements.WidgetMakingUnit");
		EntitySES firstofficer = new EntitySES("companyElements.FirstOfficer");
		
		EntitySES productiondepartment = new EntitySES("companyElements.ProductionDepartment");
		
		Aspect procdec = new Aspect("companyElements.ProcDec", productiondepartment);
		productiondepartment.addAspectToEntity(procdec);
		productiondepartment.addEntityToAspect("companyElements.ProcDec",widgetdesignunit);	
		productiondepartment.addEntityToAspect("companyElements.ProcDec",executiveoffice);	
		productiondepartment.addEntityToAspect("companyElements.ProcDec",widgetmakingunit);	
		
		Aspect bottomorgdec_widgetdesignunit = new Aspect("companyElements.BottomOrgDec", widgetdesignunit);
		productiondepartment.addAspectToEntity(bottomorgdec_widgetdesignunit);
		productiondepartment.addEntityToAspect("companyElements.BottomOrgDec",executiveoffice);	
		
		Aspect bottomorgdec_widgetmakingunit = new Aspect("companyElements.BottomOrgDec", widgetmakingunit);
		productiondepartment.addAspectToEntity(bottomorgdec_widgetmakingunit);
		productiondepartment.addEntityToAspect("companyElements.BottomOrgDec",executiveoffice);	
		
		Spec persondec_executiveoffice = new Spec("companyElements.PersonDec", executiveoffice);
		productiondepartment.addSpecToEntity(persondec_executiveoffice);
		productiondepartment.addEntityToSpec("companyElements.PersonDec",secondofficer);
		productiondepartment.addEntityToSpec("companyElements.PersonDec",firstofficer);
		//show root in XML
		productiondepartment.info(productiondepartment.toXML());
		//show entity info for root
		productiondepartment.logEntityHierarchy();

		// write SES to file (default name is ses root entity name.xml in
		// project workspace)
		productiondepartment.writeSES();
		
	
	}
	
	
	
}	
