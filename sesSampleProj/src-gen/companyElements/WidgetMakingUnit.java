package companyElements;
import GenCol.*;
import genDevs.modeling.*;
import simView.*;
import modeling.*;
import ops.*;

public class WidgetMakingUnit extends ViewableDigraph{
	
	
	public WidgetMakingUnit(){
		this("companyElements.WidgetMakingUnit");
	}
	
	public WidgetMakingUnit(String name){
		super(name);
		
			
	}
	
	public static void main(String... args){
		EntitySES secondofficer = new EntitySES("companyElements.SecondOfficer");
		EntitySES executiveoffice = new EntitySES("companyElements.ExecutiveOffice");
		EntitySES firstofficer = new EntitySES("companyElements.FirstOfficer");
		
		EntitySES widgetmakingunit = new EntitySES("companyElements.WidgetMakingUnit");
		
		Aspect bottomorgdec = new Aspect("companyElements.BottomOrgDec", widgetmakingunit);
		widgetmakingunit.addAspectToEntity(bottomorgdec);
		widgetmakingunit.addEntityToAspect("companyElements.BottomOrgDec",executiveoffice);	
		
		Spec persondec_executiveoffice = new Spec("companyElements.PersonDec", executiveoffice);
		widgetmakingunit.addSpecToEntity(persondec_executiveoffice);
		widgetmakingunit.addEntityToSpec("companyElements.PersonDec",secondofficer);
		widgetmakingunit.addEntityToSpec("companyElements.PersonDec",firstofficer);
		//show root in XML
		widgetmakingunit.info(widgetmakingunit.toXML());
		//show entity info for root
		widgetmakingunit.logEntityHierarchy();

		// write SES to file (default name is ses root entity name.xml in
		// project workspace)
		widgetmakingunit.writeSES();
		
	
	}
	
	
	
}	
