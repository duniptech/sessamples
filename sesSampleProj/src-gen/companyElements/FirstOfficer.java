package companyElements;
import GenCol.*;
import genDevs.modeling.*;
import simView.*;
import modeling.*;
import ops.*;

public class FirstOfficer extends ViewableAtomic{
	
	
	public FirstOfficer(){
		this("companyElements.FirstOfficer");
	}
	
	public FirstOfficer(String name){
		super(name);
		
			
	}
	
	public static void main(String... args){
		
		EntitySES firstofficer = new EntitySES("companyElements.FirstOfficer");
		//show root in XML
		firstofficer.info(firstofficer.toXML());
		//show entity info for root
		firstofficer.logEntityHierarchy();

		// write SES to file (default name is ses root entity name.xml in
		// project workspace)
		firstofficer.writeSES();
		
	
	}
	
	
	
}	
