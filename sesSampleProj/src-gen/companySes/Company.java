package companySes;
import GenCol.*;
import genDevs.modeling.*;
import simView.*;
import modeling.*;
import ops.*;

public class Company extends ViewableDigraph{
	
	
	public Company(){
		this("companySes.Company");
	}
	
	public Company(String name){
		super(name);
		
			
	}
	
	public static void main(String... args){
		EntitySES widgetdesignunit = new EntitySES("companyElements.WidgetDesignUnit");
		EntitySES secondofficer = new EntitySES("companyElements.SecondOfficer");
		EntitySES executiveoffice = new EntitySES("companyElements.ExecutiveOffice");
		EntitySES widgetmakingunit = new EntitySES("companyElements.WidgetMakingUnit");
		EntitySES productiondepartment = new EntitySES("companyElements.ProductionDepartment");
		EntitySES firstofficer = new EntitySES("companyElements.FirstOfficer");
		EntitySES salesdepartment = new EntitySES("companyElements.SalesDepartment");
		
		EntitySES company = new EntitySES("companySes.Company");
		
		Aspect companyorgdec = new Aspect("companyElements.CompanyOrgDec", company);
		company.addAspectToEntity(companyorgdec);
		company.addEntityToAspect("companyElements.CompanyOrgDec",executiveoffice);	
		company.addEntityToAspect("companyElements.CompanyOrgDec",productiondepartment);	
		company.addEntityToAspect("companyElements.CompanyOrgDec",salesdepartment);	
		
		Aspect bottomorgdec_widgetdesignunit = new Aspect("companyElements.BottomOrgDec", widgetdesignunit);
		company.addAspectToEntity(bottomorgdec_widgetdesignunit);
		company.addEntityToAspect("companyElements.BottomOrgDec",executiveoffice);	
		
		Aspect bottomorgdec_widgetmakingunit = new Aspect("companyElements.BottomOrgDec", widgetmakingunit);
		company.addAspectToEntity(bottomorgdec_widgetmakingunit);
		company.addEntityToAspect("companyElements.BottomOrgDec",executiveoffice);	
		
		Aspect procdec_productiondepartment = new Aspect("companyElements.ProcDec", productiondepartment);
		company.addAspectToEntity(procdec_productiondepartment);
		company.addEntityToAspect("companyElements.ProcDec",widgetdesignunit);	
		company.addEntityToAspect("companyElements.ProcDec",executiveoffice);	
		company.addEntityToAspect("companyElements.ProcDec",widgetmakingunit);	
		
		Aspect bottomorgdec_salesdepartment = new Aspect("companyElements.BottomOrgDec", salesdepartment);
		company.addAspectToEntity(bottomorgdec_salesdepartment);
		company.addEntityToAspect("companyElements.BottomOrgDec",executiveoffice);	
		
		Spec persondec_executiveoffice = new Spec("companyElements.PersonDec", executiveoffice);
		company.addSpecToEntity(persondec_executiveoffice);
		company.addEntityToSpec("companyElements.PersonDec",secondofficer);
		company.addEntityToSpec("companyElements.PersonDec",firstofficer);
		//show root in XML
		company.info(company.toXML());
		//show entity info for root
		company.logEntityHierarchy();

		// write SES to file (default name is ses root entity name.xml in
		// project workspace)
		company.writeSES();
		
	
	}
	
	
	
}	
